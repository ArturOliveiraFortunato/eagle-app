# eagle_stream

A new Flutter application.

## How To Run
### In Production Mode
`flutter run`

### In Production, Staging or Development Mode
`flutter run --dart-define=env="ENVIRONMENT"`, where `ENVIRONMENT` can take one of the following values `{dev, staging, prod}`.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
