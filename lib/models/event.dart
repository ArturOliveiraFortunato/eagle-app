import 'dart:typed_data';

import 'package:eagle_stream/models/sport.dart';
import 'package:eagle_stream/models/statistics.dart';
import 'package:eagle_stream/models/team.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:flutter/material.dart';

class Event {
  final String id;
  final Sport sport;
  final DateTime date;
  final Team team1;
  final Team team2;
  Uint8List image;
  final bool live;
  //final String zone;
  Statistics stats;
  final String location;
  final String competition;

  Event({
    @required this.id,
    @required this.sport,
    @required this.date,
    @required this.team1,
    @required this.team2,
    this.image,
    @required this.live,
    //@required this.zone,
    this.stats,
    @required this.location,
    @required this.competition,
  });

  factory Event.fromJson(Map<dynamic, dynamic> json) {
    dynamic date = json['date'];

    DateTime dateTime = DateTime(
        int.parse(date['year']),
        int.parse(date['month']),
        int.parse(date['day']),
        int.parse(date['hours']),
        int.parse(date['minutes']));

    List<String> teams = json['teams'].split('|');
    return Event(
      id: json['id'].toString(),
      sport: Sport(name: json['sport'], stats: sportsStats[json['sport']]),
      date: dateTime,
      team1: Team(name: teams[0]),
      team2: Team(name: teams[1]),
      live: json['state'] == "ONGOING",
      location: json['location'],
      competition: json['competition'],
    );
  }

  void updateStatistics(Statistics statistics) {
    this.stats = statistics;
  }

  void updateStat(String stat, int valueTeam1, int valueTeam2) {
    stats.statsTeam1[stats.statsNames.indexOf(stat)] = valueTeam1;
    stats.statsTeam2[stats.statsNames.indexOf(stat)] = valueTeam2;
  }

  @override
  String toString() {
    return (this.id +
        "-" +
        this.sport.name +
        " " +
        this.team1.name +
        "|" +
        this.team2.name +
        " " +
        this.competition +
        " " +
        this.location);
  }
}
