import 'package:flutter/cupertino.dart';

class Sport {
  String name;
  List<String> stats;

  Sport({
    @required this.name,
    @required this.stats,
  });
}
