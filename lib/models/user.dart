import 'package:flutter/material.dart';

class User {
  String name;
  String address;
  String email;
  String birthday;
  bool isAdmin;

  User(
      {@required this.name,
      @required this.address,
      @required this.email,
      @required this.birthday,
      @required this.isAdmin});

  factory User.fromJson(Map<dynamic, dynamic> json) {
    return User(
        name: json['username'],
        email: json['email'],
        address: "Lisboa, Portugal", //placeholder
        birthday: "01 / 07 / 1997",
        isAdmin: json['authority'] == "ROLE_ADMIN"); //placeholder
  }
}
