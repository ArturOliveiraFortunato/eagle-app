import 'dart:typed_data';
import 'package:flutter/material.dart';

class Team {
  final Uint8List image;
  final String name;

  Team({
    @required this.image,
    @required this.name,
  });
}
