import 'package:flutter/material.dart';

class Statistics {
  List<String> statsNames;
  List<dynamic> statsTeam1;
  List<dynamic> statsTeam2;
  // final List<int> minValues;
  // final List<int> maxValues;
  final String team1Name;
  final String team2Name;
  int watchers;

  Statistics({
    @required this.statsNames,
    @required this.statsTeam1,
    @required this.statsTeam2,
    // @required this.minValues,
    // @required this.maxValues,
    @required this.team1Name,
    @required this.team2Name,
  });

  void setWatcher(int watchers) {
    this.watchers = watchers;
  }

  void setStatsNames(List<String> statsNames) {
    this.statsNames = statsNames;
  }

  void setTeam1Stats(List<dynamic> statsTeam1) {
    this.statsTeam1 = statsTeam1;
  }

  void setTeam2Stats(List<dynamic> statsTeam2) {
    this.statsTeam2 = statsTeam2;
  }
}
