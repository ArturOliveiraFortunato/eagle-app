import 'package:flutter/material.dart';

const Color salmon = const Color(0xFFEF3E43);

const Color black = const Color(0xFF000000);
const Color dark = const Color(0xFF272727);
const Color grey = const Color(0xFF707070);
const Color light_grey = const Color(0xFFC6C6C6);
const Color lighter_grey = const Color(0xFFF7F7F7);
const Color white = const Color(0xFFFFFFFF);

const Color orange = const Color(0xFFE77927);
const Color red = const Color(0xFFE0171D);
