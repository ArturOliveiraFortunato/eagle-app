import 'package:flutter/material.dart';
import 'package:eagle_stream/style/custom_colors.dart';

ThemeData theme = ThemeData(
  visualDensity: VisualDensity.adaptivePlatformDensity,
  brightness: Brightness.light,
  canvasColor: black,
  primaryColor: salmon,
  accentColor: grey,
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    type: BottomNavigationBarType.fixed,
    selectedItemColor: salmon,
    unselectedItemColor: white,
    showSelectedLabels: false,
    showUnselectedLabels: false,
    backgroundColor: dark,
  ),
);
