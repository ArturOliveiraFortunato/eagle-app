import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/cupertino.dart';

const TextStyle roboto_light_big = const TextStyle(
  fontFamily: 'Roboto-Light',
  fontSize: 12,
  color: white,
);

const TextStyle roboto_medium_big = const TextStyle(
  fontFamily: 'Roboto-Medium',
  fontSize: 12,
  color: white,
);

const TextStyle roboto_medium_small = const TextStyle(
  fontFamily: 'Roboto-Medium',
  fontSize: 11,
  color: white,
);

const TextStyle gotham_book_big = const TextStyle(
  fontFamily: 'Gotham-Book',
  fontSize: 18,
  color: white,
);

const TextStyle gotham_book_medium = const TextStyle(
  fontFamily: 'Gotham-Book',
  fontSize: 14,
  color: white,
);

const TextStyle gotham_book_small = const TextStyle(
  fontFamily: 'Gotham-Book',
  fontSize: 11,
  color: white,
);

const TextStyle gotham_bold_small = const TextStyle(
  fontFamily: 'Gotham-Bold',
  fontSize: 11,
  color: white,
);

const TextStyle gotham_bold_medium = const TextStyle(
  fontFamily: 'Gotham-Bold',
  fontSize: 15,
  color: white,
);

const TextStyle gotham_bold_big = const TextStyle(
  fontFamily: 'Gotham-Bold',
  fontSize: 23,
  color: white,
);

const TextStyle gotham_medium_big = const TextStyle(
  fontFamily: 'Gotham-Medium',
  fontSize: 18,
  color: white,
);

const TextStyle gotham_medium_small = const TextStyle(
  fontFamily: 'Gotham-Bold',
  fontSize: 11,
  color: white,
);

const TextStyle gotham_medium_bigger = const TextStyle(
  fontFamily: 'Gotham-Bold',
  fontSize: 30,
  color: white,
);
