import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget playerName({@required String name, @required double size}) {
  return Container(
    height: size * 0.4,
    width: size,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.transparent,
      border: Border.all(color: white),
    ),
    child: Center(
      child: Container(
        height: size * 0.3,
        width: size * 0.9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3)),
          color: white,
          border: Border.all(color: white),
        ),
        child: Center(
          child: Text(
            name,
            style: size == 100
                ? gotham_bold_medium.copyWith(color: grey)
                : gotham_bold_small.copyWith(color: grey),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
    ),
  );
}
