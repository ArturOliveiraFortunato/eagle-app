import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class TextFieldAutoComplete2 extends StatefulWidget {
  final List<String> suggestions;
  final String hint;
  final String value;
  final Function onChange;

  TextFieldAutoComplete2({
    Key key,
    @required this.suggestions,
    @required this.value,
    @required this.hint,
    @required this.onChange,
  }) : super(key: key);
  @override
  _TextFieldAutoComplete2State createState() => _TextFieldAutoComplete2State();
}

class _TextFieldAutoComplete2State extends State<TextFieldAutoComplete2> {
  var _suggestionsTextFieldController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return TypeAheadField(
      noItemsFoundBuilder: (value) {
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            "Não foram encontradas equipas com esse nome.",
            style: gotham_book_medium.copyWith(color: black),
          ),
        );
      },
      suggestionsBoxDecoration: SuggestionsBoxDecoration(
          color: white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          )),
      textFieldConfiguration: TextFieldConfiguration(
        controller: _suggestionsTextFieldController,
        style: gotham_book_medium.copyWith(color: black),
        decoration: InputDecoration(
          fillColor: white,
          filled: true,
          hintText: widget.hint,
          hintStyle: TextStyle(fontSize: 12, fontFamily: 'Gotham-Book'),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      suggestionsCallback: (pattern) async {
        return await ServerInterface.serverInterface.getTeamsPattern(pattern);
      },
      onSuggestionSelected: (item) {
        _suggestionsTextFieldController.text = item;
        widget.onChange(item);
      },
      itemBuilder: (context, item) {
        return Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            item,
            style: gotham_book_medium.copyWith(color: black),
          ),
        );
      },
    );
  }
}
