import 'package:flutter/widgets.dart';

Widget logo({@required double height}) {
  return Image.asset(
    'assets/images/logo_nocap.png',
    height: height,
  );
}
