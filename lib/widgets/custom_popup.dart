import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

Future<bool> customPopup(
    BuildContext context, String text, List<DialogButton> buttons) {
  return Alert(
          context: context,
          title: '',
          style: AlertStyle(
            backgroundColor: black,
            isCloseButton: false,
          ),
          content: Column(
            children: <Widget>[
              Text(
                text,
                style: gotham_book_medium,
              ),
            ],
          ),
          buttons: buttons)
      .show();
}
