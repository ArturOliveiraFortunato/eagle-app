import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/material.dart';

Widget playButton({@required Function onPressed}) {
  return Container(
    decoration: const ShapeDecoration(
      color: salmon,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
    ),
    child: IconButton(
      icon: Icon(Icons.play_arrow_rounded, color: white),
      onPressed: onPressed,
      iconSize: 50,
      padding: EdgeInsets.all(3),
    ),
  );
}
