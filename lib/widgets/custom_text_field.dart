import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/material.dart';

Widget customTextFiled(
    {@required String hint,
    @required bool hideText,
    @required onChanged,
    String text}) {
  return Container(
    height: 40,
    child: TextFormField(
      enabled: onChanged != null,
      initialValue: text,
      obscureText: hideText,
      decoration: InputDecoration(
        fillColor: white,
        filled: true,
        hintText: hint,
        hintStyle: TextStyle(fontSize: 12, fontFamily: 'Gotham-Book'),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          borderSide: BorderSide(color: Colors.grey),
        ),
      ),
      onChanged: onChanged,
    ),
  );
}
