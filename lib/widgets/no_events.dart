import 'package:flutter/material.dart';

import '../style/custom_colors.dart';
import '../style/typography.dart';

Widget noEvents(String text) {
  return Padding(
    padding: const EdgeInsets.only(top: 40, bottom: 40),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(height: 60, child: Image.asset('assets/images/no-games.png')),
        SizedBox(width: 10),
        Text(text, style: gotham_bold_small.copyWith(color: grey)),
      ],
    ),
  );
}
