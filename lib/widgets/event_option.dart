import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/screens/event/event_screen.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:eagle_stream/widgets/custom_popup.dart';
import 'package:eagle_stream/widgets/player_name.dart';
import 'package:eagle_stream/widgets/team_emblem.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

String printHour(DateTime time) {
  String hour =
      time.hour < 10 ? "0" + time.hour.toString() : time.hour.toString();
  String minute =
      time.minute < 10 ? "0" + time.minute.toString() : time.minute.toString();
  return hour + ":" + minute;
}

Widget eventOption(
    {@required Event event,
    @required bool big,
    @required BuildContext context}) {
  ServerInterface serverInterface = ServerInterface.getInstance();
  return GestureDetector(
    onTap: () => Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EventScreen(event: event)),
    ),
    onLongPress: () {
      customPopup(context, "Tem a certeza que deseja eliminar este evento?", [
        DialogButton(
          color: black,
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            "NÃO",
            style: gotham_book_medium,
          ),
        ),
        DialogButton(
          color: salmon,
          onPressed: () {
            serverInterface.deleteEvent(event.id);
            Navigator.pop(context);
          },
          child: Text(
            "SIM",
            style: gotham_book_medium,
          ),
        ),
      ]);
    },
    child: Container(
      width: big ? 300 : 200,
      height: big ? 200 : 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: black,
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: big ? 300 : 200,
            height: big ? 200 : 150,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.network(
                "https://aws-eagle-stream-app-website-logos.s3.eu-west-3.amazonaws.com/Sports/" +
                    event.sport.name.toLowerCase() +
                    ".jpg",
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: big ? 300 : 200,
            height: big ? 200 : 150,
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: FractionalOffset.center,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.black.withOpacity(0.0),
                Colors.black.withOpacity(0.6),
              ],
            )),
          ),
          event.live
              ? Positioned(
                  left: 10,
                  top: 0,
                  child: customButton(
                      text: "LIVE",
                      onClick: null,
                      icon: Icons.fiber_manual_record))
              : Positioned(
                  bottom: big ? 15 : 5,
                  child: Column(
                    children: [
                      Text(printHour(event.date), style: gotham_medium_big),
                      SizedBox(
                        height: 5,
                      ),
                      Text("${event.date.day} ${months[event.date.month]}",
                          style: gotham_medium_small.copyWith(fontSize: 13)),
                    ],
                  ),
                ),
          if (["volleyball", "football", "basketball", "handball"]
              .contains(event.sport.name))
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TeamEmblem(
                    teamName: event.team1.name.toLowerCase(),
                    size: big ? 70 : 50),
                SizedBox(width: big ? 70 : 50),
                TeamEmblem(
                    teamName: event.team2.name.toUpperCase(),
                    size: big ? 70 : 50),
              ],
            ),
          if (["tennis", "judo", "padel"].contains(event.sport.name))
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                playerName(name: event.team1.name, size: big ? 100 : 70),
                SizedBox(width: big ? 70 : 50),
                playerName(name: event.team2.name, size: big ? 100 : 70),
              ],
            ),
          Icon(
            Icons.play_arrow_rounded,
            color: white,
            size: big ? 60 : 40,
          ),
        ],
      ),
    ),
  );
}
