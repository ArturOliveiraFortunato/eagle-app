import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget customButton({
  @required String text,
  @required Function onClick,
  IconData icon,
}) {
  return Container(
    child: TextButton(
        onPressed: onClick,
        style: TextButton.styleFrom(
          minimumSize: Size(0, 0),
          padding: EdgeInsets.only(
              top: 6, bottom: 6, left: 8, right: text == null ? 8 : 11),
          primary: white,
          backgroundColor: salmon,
        ),
        child: icon == null
            ? Text(
                text,
                style: gotham_medium_small,
              )
            : text == null
                ? Icon(
                    icon,
                    color: white,
                    size: text == "LIVE" ? 8 : null,
                  )
                : Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        icon,
                        color: white,
                        size: text == "LIVE" ? 8 : null,
                      ),
                      SizedBox(width: 2),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Text(
                          text,
                          style: text == "LIVE"
                              ? gotham_medium_small.copyWith(fontSize: 8)
                              : gotham_medium_small,
                        ),
                      )
                    ],
                  )),
  );
}
