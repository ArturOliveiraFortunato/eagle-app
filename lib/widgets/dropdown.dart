import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget dropdown(
    {@required String value,
    @required List<String> items,
    @required onChanged,
    String hint}) {
  return DropdownButton<String>(
    hint: hint != null
        ? Container(
            width: 100,
            child: Text(
              hint,
              style: gotham_medium_small,
              overflow: TextOverflow.ellipsis,
            ))
        : null,
    value: value != null ? value : null,
    elevation: 5,
    items: items.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(
          value,
          style: gotham_medium_small,
        ),
      );
    }).toList(),
    onChanged: (String newValue) {
      onChanged(newValue);
    },
  );
}
