import 'dart:typed_data';

import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/material.dart';

class TeamEmblem extends StatelessWidget {
  final Uint8List emblem;
  final AssetImage image;
  final double size;
  final String teamName;

  TeamEmblem({
    Key key,
    this.emblem,
    this.image,
    this.teamName,
    @required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        color: Colors.transparent,
        shape: BoxShape.circle,
        border: Border.all(width: 1, color: white),
      ),
      child: Center(
        child: Container(
          height: size * 0.75,
          width: size * 0.75,
          decoration: BoxDecoration(
            color: white,
            shape: BoxShape.circle,
          ),
          child: Center(
            child: emblem != null
                ? Image.memory(
                    emblem,
                    width: size * 0.70,
                    height: size * 0.70,
                  )
                : image != null
                    ? CircleAvatar(
                        backgroundImage: image,
                        minRadius: size * 0.75,
                      )
                    : teamName != null
                        ? CircleAvatar(
                            maxRadius: size * 0.3,
                            backgroundColor: white,
                            backgroundImage: NetworkImage(
                              "https://aws-eagle-stream-app-website-logos.s3.eu-west-3.amazonaws.com/Teams/universities_pt/" +
                                  (teamName == "NOVA SBE"
                                      ? "nova_sbe"
                                      : teamName.toLowerCase()) +
                                  ".png",
                            ),
                          )
                        : CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
