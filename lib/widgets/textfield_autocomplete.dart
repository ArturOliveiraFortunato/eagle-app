import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:flutter/material.dart';

class TextFieldAutoComplete extends StatefulWidget {
  final List<String> suggestions;
  final String hint;
  final String value;
  final Function onChange;

  TextFieldAutoComplete({
    Key key,
    @required this.suggestions,
    @required this.value,
    @required this.hint,
    @required this.onChange,
  }) : super(key: key);
  @override
  _TextFieldAutoCompleteState createState() => _TextFieldAutoCompleteState();
}

class _TextFieldAutoCompleteState extends State<TextFieldAutoComplete> {
  var _suggestionsTextFieldController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return AutoCompleteTextField(
        decoration: InputDecoration(
          fillColor: white,
          filled: true,
          hintText: widget.hint,
          hintStyle: TextStyle(fontSize: 12, fontFamily: 'Gotham-Book'),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
        controller: _suggestionsTextFieldController,
        suggestions: widget.suggestions,
        style: gotham_book_medium.copyWith(color: black),
        itemSorter: (a, b) {
          return a.compareTo(b);
        },
        itemSubmitted: (item) {
          _suggestionsTextFieldController.text = item;
          widget.onChange(item);
        },
        itemBuilder: (context, item) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              item,
              style: gotham_book_medium.copyWith(color: black),
            ),
          );
        },
        itemFilter: (item, query) {
          return item.toLowerCase().startsWith(query.toLowerCase());
        });
  }
}
