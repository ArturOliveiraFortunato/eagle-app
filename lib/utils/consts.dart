const List<String> zones = [
  "Lisboa",
  "Coimbra",
  "Porto",
  "Setúbal",
];

const List<String> sports = [
  "tennis",
  "volleyball",
  "padel",
];

const Map<String, String> ptToEng = {
  "pontos": "Points",
  "serviços": "Services",
  "ataques": "Attacks",
  "bloqueios": "Blocks",
  "faltas": "Faults",
  "sets": "Sets",
};

const Map<String, String> engToPt = {
  "points": "Pontos",
  "services": "Serviços",
  "attacks": "Ataques",
  "blocks": "Bloqueios",
  "faults": "Faltas",
  "sets": "Sets",
};

const Map<String, List<String>> sportsStats = {
  "basketball": [
    "Ataques",
    "Lances Livres",
    "Lances Triplo",
    "Faltas",
  ],
  "tennis": [
    "Sets",
    "Games",
    "Pontos",
    "Serviços",
    "Faltas",
    "Duplas Faltas",
    "Deuce",
    "Às",
    "Vantagem",
    "Tie-Breaker",
    "Let"
  ],
  "football": [
    "Faltas",
    "Cartões Amarelos",
    "Expulsões",
    "Livres",
    "Cantos",
    "Remates",
    "Pénaltis",
  ],
  "volleyball": [
    "Pontos",
    "Sets",
    "Serviços",
    "Ataques",
    "Bloqueios",
    "Faltas",
  ],
  "judo": [
    "Advertências",
    "Yuko",
    "Waza-Ari",
    "Ippon",
  ],
};

const Map<String, List<String>> newEventSections = {
  "MODALIDADE": [
    "PADEL",
    "TENNIS",
    "VOLLEYBALL",
  ],
  "EQUIPAS / ATLETAS": [
    "EQUIPA 1",
    "EQUIPA 2",
    "EQUIPA 3",
    "EQUIPA 4",
    "EQUIPA 5",
    "ATLETA 1",
    "ATLETA 2",
    "ATLETA 3",
    "ATLETA 4",
    "ATLETA 5",
  ],
  "COMPETIÇÃO": [
    "CAMPEONATO 1",
    "CAMPEONATO 2",
    "CAMPEONATO 3",
    "CAMPEONATO 4",
    "CAMPEONATO 5",
    "CAMPEONATO 6",
  ],
  "ESCALÃO": [
    "SUB 13",
    "SUB 15",
    "SUB 17",
    "SUB 19",
    "SUB 21",
    "SENIOR",
  ],
};

List<String> policyTopics = [
  "Validade da Política de Privacidade",
  "Processamento de dados pessoais",
  "Cookies",
  "Google Analytics",
  "Uso de Facebook, Twitter e Google",
  "Proprietário do arquivo de dados",
  "Direito de acesso e correção de dados"
];

Map<String, List<String>> policySubTopics = {
  "Validade da Política de Privacidade": [
    'Eagle Software Lda. ("Eagle Stream") opera o site www.eagle-stream.com e a App Eagle Stream disponibilizada na App Store e Google Play.',
    'Toda vez que um utilizador aceder ao Site, ele ou ela concorda com a presente Política de Privacidade ("Política de Privacidade") na versão válida no momento do acesso, com relação ao Eagle Stream.',
    'Esta Política de Privacidade é parte integral dos Termos de Uso ("TdU") do Eagle Stream.',
    'Regulamentos estatutários de privacidade de dados aplicar-se-ão juntamente com esta Política de Privacidade.'
  ],
  "Processamento de dados pessoais": [
    'Em conexão com o uso de cada serviço do site, o utilizador pode transmitir dados pessoais ("dados") para o Eagle Stream e/ou para provedores de serviço mobilizados pelo Eagle Stream (como prestadores de portais de pagamento ou parceiros de marketing). Tais dados podem abranger, entre outros, número de telefone, endereço de e-mail ou informações bancárias e de cartão de crédito. Além disso, podem ser armazenadas data e hora do acesso, o endereço do site acedido, o endereço de IP do dispositivo, o URL a partir do qual o utilizador acedeu ao site, o volume de dados transmitidos e informações sobre o tipo e a versão do navegador que você estiver utilizando.',
    'Se o utilizador revelar dados pertencentes a terceiros para o Eagle Stream, o utilizador é responsável pela aprovação dos teores dos referidos dados e pela exatidão das informações prestadas',
    'O Eagle Stream precisa dos dados dos utilizadores para os fins de administração de assinaturas e processamento de transações de pagamento, atendimento ao consumidor, administração técnica e para os seus próprios fins comerciais. Os dados serão revelados a terceiros apenas e se no grau exigido pelos serviços prestados por meio do site, para.',
    'Avaliação de crédito, para a conclusão de contrato com um provedor externo providenciado pelo Eagle Stream ou para o processamento de transações de pagamento, ou se e no grau em que você consentiu previamente.',
    'O Eagle Stream garante privacidade de dados adequada em conformidade com requisitos estatutários. Especificamente, o Eagle Stream protege o site e todos os demais sistemas de maneira adequada por meio de medidas técnicas e organizacionais contra perda, destruição, acesso, modificação ou divulgação de dados por terceiros não autorizados.',
    'O Eagle Stream reserva-se ao direito de guardar os dados utilizador até que seja solicitada a sua eliminação por escrito. Exigências estatutárias de retenção de dados permanecem reservadas.',
  ],
  "Cookies": [
    'Este site usa "cookies". Cookies são pequenos arquivos de texto enviados ao navegador do usuário pelo servidor web correspondente na primeira visita ao site e armazenados no computador do usuário ou no cache do navegador. Quando o mesmo dispositivo aceder ao site, o navegador verifica se um cookie correspondente já está disponível. O navegador envia os dados armazenados no cookie de volta para o servidor web. Dessa forma, o servidor web pode detetar se o navegador já visitou o mesmo site.',
    'Os dados coletados com a ajuda de cookies são usados pelo Eagle Stream meramente para melhorar os seus serviços e para analisar anonimamente o uso do site. Não há outra utilização dos dados, tampouco os dados são utilizados para identificar o usuário deste site.',
    'O utilizador pode configurar o seu navegador para não armazenar quaisquer cookies deste site no seu disco rígido ou fazer com que o seu navegador apague os cookies já gravados no seu dispositivo. No entanto, deve ter em mente que, em assim fazendo, pode não conseguir usar por completo todas as funções fornecidas no site. O procedimento para alterar essas configurações varia de navegador para navegador; informações referentes a adaptações e à definição das suas configurações podem ser encontradas no menu de "Ajuda" de cada navegador.',
  ],
  "Google Analytics": [
    'A aplicação Eagle Stream utiliza o Google Analytics, um serviço de análise da web prestado pelo Google Inc., 1600 Amphitheatre Parkway, Mountain View, California 94043, EUA ("Google"). O Google Analytics usa os chamados "cookies", isto é, pequenos arquivos de texto gravados nos computadores dos usuários que possibilitam a análise do uso da web. As informações criadas por esse cookie com relação ao uso deste site (inclusive endereços de IP) são enviadas para um servidor do Google nos EUA, e nele armazenadas. O Google utilizará essas informações para analisar o uso deste site, compilar relatórios sobre o tráfego da web para os proprietários do site e prestar outros serviços relacionados ao uso do site e da internet. Além disso, o Google enviará essas informações para terceiros somente se exigido por lei ou se terceiros processarem essas informações em nome do Google. O endereço de IP revelado pelo navegador do usuário no contexto do Google Analytics não será combinado com todos os outros dados compilados pelo Google.',
    'Uma configuração específica dos navegadores permite que os usuários impeçam o armazenamento de cookies; no entanto, eles são lembrados de que não poderão mais usar todas as funções por completo. Além disso, os usuários podem impedir a captura dos dados criados pelos cookies e relacionados ao seu uso do site, assim como o processamento dos dados pelo Google, baixando e instalando o plug-in a seguir, encontrado no link abaixo: http://tools.google.com/dlpage/gaoptout?hl=de.',
    'Ao utilizar este site, o utilizador concorda com o processamento dos seus dados coletados pelo Google da maneira descrita acima e para o fim supracitado.',
  ],
  "Uso de Facebook, Twitter e Google": [
    'O Eagle Stream utiliza plug-ins das redes sociais ("plug-ins") do facebook.com, operado por Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, EUA ("Facebook") e do twitter.com, operado por Twitter Inc., 795 Folsom Street, Suite 600, San Francisco, CA 94107, EUA ("Twitter"); Facebook, Instagram e Twitter são doravante chamados de "Redes Sociais"). Os plug-ins são identificados pelo ícone da respetiva rede social.',
    'Quando você visita um site que contém plug-ins, o seu navegador estabelece uma conexão direta com os servidores da respetiva rede social. O conteúdo dos plug-ins é transmitido diretamente da rede social para o seu navegador e vinculado ao site.',
    'Se o utilizador estiver conectado à rede social correspondente, pode-se atribuir o acesso do site à sua conta da respetiva rede social. Quando você interage com os plug-ins, por exemplo, com o botão "Curtir" do Facebook, ou faz um comentário, a informação correspondente é transmitida do seu navegador diretamente para a rede social correspondente.',
    'Por favor, consulte as políticas de privacidade de dados de Facebook, Instagram ou Twitter com relação à finalidade e ao grau da coleta, processamento e uso de dados pelas redes sociais correspondentes, bem como os direitos nesse aspeto e as opções de configurações para a proteção da sua privacidade.',
  ],
  "Proprietário do arquivo de dados": [
    'A Eagle Software é proprietária dos dados.'
  ],
  "Direito de acesso e correção de dados": [
    'O utilizador tem direito ao acesso e/ou à correção dos seus dados, de acordo com disposições estatutárias. Uma solicitação para o acesso ou a correção deve ser enviada por escrito ou por e-mail, acompanhada da cópia de um documento de identificação, para o seguinte endereço: Eagle Software eaglestream88@gmail.com'
  ]
};

const months = <String>[
  'Jan',
  'Feb',
  'Mar',
  'Abr',
  'Mai',
  'Jun',
  'Jul',
  'Ago',
  'Set',
  'Out',
  'Nov',
  'Dec',
];

const months2 = <String>[
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];
