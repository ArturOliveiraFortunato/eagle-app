import 'package:eagle_stream/screens/login/login_screen.dart';
import 'package:eagle_stream/screens/main/main_screen.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'models/user.dart';

Future main() async {
  // Environment setup
  const env = String.fromEnvironment('env', defaultValue: 'prod');

  if(env == "dev") {
    await DotEnv().load('.env_dev');
  } else if(env == "staging") {
    await DotEnv().load('.env_staging');
  } else {
    //await DotEnv().load('.env_prod');
    await DotEnv().load('.env_dev');
  }
  // Environment setup - end

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool loggedIn = false;
  bool stayLogged = false;
  bool failedLogin = false;
  bool checkedLogin = false;
  Map<String, Function> handler;
  User user;

  final ServerInterface serverInterface = ServerInterface.getInstance();

  void toggleStayLogged(bool value) {
    setState(() {
      stayLogged = value;
    });
  }

  void failLogin() {
    setState(() {
      failedLogin = true;
    });
  }

  void login({@required String username, @required String password}) {
    Map<String, String> credentials = {
      'username': username,
      'password': password
    };

    serverInterface
        .login(credentials, stayLogged)
        .then((_) => {
              setState(() {
                loggedIn = true;
                checkedLogin = true;
              })
            })
        .catchError((error) {
      print(error);
      setState(() {
        checkedLogin = true;
        failedLogin = true;
      });
    });
  }

  void logout() {
    setState(() {
      loggedIn = false;
    });
    serverInterface.logout();
  }

  void onCreateAccount(
      {@required String email,
      @required String username,
      @required String password}) {
    serverInterface
        .signUp(username: username, password: password, email: email)
        .then((response) {
      print(response);
      if (response == "User Registered") {
        Navigator.pop(context);
        setState(() {
          loggedIn = true;
        });
      }
    });
  }

  void onForgetPassword({@required String email}) {
    serverInterface.forgotPassword(email);
  }

  @override
  void initState() {
    precacheImage(AssetImage('assets/images/logo_3.png'), context);
    super.initState();
    handler = <String, Function>{
      "login": login,
      "forgot_password": onForgetPassword,
      "create_account": onCreateAccount,
      "toggle_stay_logged": toggleStayLogged,
    };

    serverInterface.foundSavedCredentials().then((isLoggedIn) {
      if (isLoggedIn) {
        serverInterface.getUserInfo().then((fetchedUser) {
          if (fetchedUser != 403)
            setState(() {
              loggedIn = isLoggedIn;
              user = fetchedUser;
            });
          setState(() {
            checkedLogin = true;
          });
        });
      } else {
        setState(() {
          checkedLogin = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate
        ],
        supportedLocales: [
          const Locale('pt'),
        ],
        theme: theme,
        home: checkedLogin
            ? loggedIn
                ? MainScreen(onLogout: logout)
                : LoginScreen(handler: handler, status: {
                    'failedLogin': failedLogin,
                    'stayLogged': stayLogged
                  })
            : Center(child: Container(child: CircularProgressIndicator())));
  }
}
