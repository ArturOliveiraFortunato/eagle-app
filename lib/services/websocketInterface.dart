import 'dart:convert';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

/*
* WebsocketInterface
* WebsocketInterface websocketInterface = new WebsocketInterface();
*/
class WebsocketInterface {
  StompClient stompClient;

  String eventId;
  Function onNewMessageCallback;

  //add security from here
  final ServerInterface serverInterface = ServerInterface.getInstance();

  /*
  * sendMessage
  *
  * @param path: string with endpoint
  * @param update: map with info to update
  */
  void sendMessage(String path, Map<String, dynamic> update) {
    stompClient.send(
        destination: '/ws-events/event/' + eventId + path,
        body: json.encode(update));
  }

  /*
  * onConnect
  *
  * @param client: StompClient
  * @param frame: StompFrame
  */
  void onConnect(StompClient client, StompFrame frame) {
    client.subscribe(
      destination: '/ws-events/event/' + eventId,
      callback: onNewMessage,
    );
  }

  /*
  * close
  *
  * Closes StompClient connection
  */
  void close() {
    stompClient.deactivate();
  }

  /*
  * onNewMessage
  *
  * @param frame: StompFrame
  */
  void onNewMessage(StompFrame frame) {
    Map<String, dynamic> jsonMap = json.decode(frame.body);
    // print("message");
    print(jsonMap);
    // print(jsonMap["messageType"].toString().toLowerCase());
    // String stat =
    //     engToPt[jsonMap["messageType"].toString().toLowerCase()].toLowerCase();
    // Map<String, dynamic> score = jsonMap["payload"]["score"];
    // print(score.values.first);
    Map<String, dynamic> stats = jsonMap["payload"]["stats"];
    print(stats);
    onNewMessageCallback(stats);
  }

  /*
  * openConnection
  *
  * @param id: String
  * @param onNewMessage2: Callback function
  */
  void openConnection(String id, Function onNewMessage2) {
    eventId = id;
    onNewMessageCallback = onNewMessage2;

    stompClient = StompClient(
      config: StompConfig.SockJS(
        url: 'https://websocket.eaglestream.tv/ws-connect',
        onConnect: onConnect,
        onDebugMessage: (dynamic msg) =>
            print("[STOMP]DEBUG " + msg.toString()),
        onStompError: (frame) => print("[STOMP] ERROR " + frame.toString()),
      ),
    );

    stompClient.activate();
  }

  void changePoints(String team, int points) {
    print("points");
    sendMessage("/updatePoints", {"team": team, "score": points});
  }

  void changeSet(String team, int set) {
    sendMessage("/set", {"team": team, "set": set});
  }

  void updateState(String eventID, Map<String, dynamic> state) {
    sendMessage(
        "/event/$eventId/update", {"type": "UPDATE-STATE", "state": state});
  }

  void changeEventState(Map<String, dynamic> state, String eventId) {
    print("message");
    print(state);
    sendMessage("/update", state);
  }
}
