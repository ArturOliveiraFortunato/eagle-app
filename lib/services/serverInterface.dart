import 'dart:convert';
import 'dart:io';
import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

/*
* ServerInterface
* Singleton:  ServerInterface serverInterface = ServerInterface.getInstance();
*/
class ServerInterface {
  ServerInterface._internal() {
    if (ioClient == null) setIoClient();
  }

  static final ServerInterface serverInterface = ServerInterface._internal();

  Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8'
  };

  final String serverDomainName = 'https://backend.eaglestream.tv';
  final String webSocketServerDomainName = 'https://websocket.eaglestream.tv';
  final String env = "PRODUCTION";

  IOClient ioClient;

  final storage = new FlutterSecureStorage();
  bool saveCredentials = false;
  /*
  * Returns this ServerInterface
  */
  static ServerInterface getInstance() {
    return serverInterface;
  }

  Future<String> getEnvURL(String source) async {
    return "https://" + DotEnv().env[source];
  }

  void setIoClient() {
    getIoClient().then((newIoClient) {
      ioClient = newIoClient;
    });
  }

  Future<IOClient> getIoClient() async {
    HttpClient httpClient = new HttpClient();
    return new IOClient(httpClient);
  }

  /*
  * getMyUsername
  *
  * Returns username from Jwt
  */
  String getMyUsername() {
    return JwtDecoder.decode(headers['Cookie'])["sub"];
  }

  /*
  * foundSavedCredentials
  *
  * Checks if the credentials are stored locally
  */
  Future<bool> foundSavedCredentials() async {
    setIoClient();
    return storage.read(key: "jwt").then((jwt) {
      if (jwt != null && JwtDecoder.isExpired(jwt.split("=")[1]) == false) {
        headers['Cookie'] = jwt;
        return true;
      }
      return false;
    });
  }

  /*
  * saveCookie
  *
  * Stores credentials locally
  */
  void saveCookie(response, saveCredentials) {
    List<String> cookieLst;

    if (response.headers['set-cookie'] != null) {
      cookieLst = response.headers['set-cookie'].split(";")[0].split("=");
      headers['Cookie'] = cookieLst[0] + "=" + cookieLst[1];
    } else {
      throw Exception('Bad credentials');
    }

    if (saveCredentials) storage.write(key: "jwt", value: headers['Cookie']);
  }

  void deleteCookie() {
    storage.delete(key: 'jwt');
    headers.remove('set-cookie');
  }

  Future<String> signUp(
      {@required String username,
      @required String password,
      @required String email}) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");
    await ioClient
        .post(
          url + "/signup",
          headers: headers,
          body: jsonEncode(
              {"username": username, "password": password, "email": email}),
        )
        .then((response) => json.decode(json.encode(response.body)))
        .catchError((e) => throw Exception(e));
  }

  /*
  * login
  *
  * @param path: string with login endpoint
  * @param credentials: map with credentials (username, password)
  * @param saveCredentials: bool
  */
  Future<void> login(Map credentials, bool saveCredentials) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    await ioClient
        .post(
      url + "/authenticate",
      headers: headers,
      body: jsonEncode(credentials),
    )
        .then((response) {
      this.saveCredentials = saveCredentials;
      saveCookie(response, saveCredentials);
    }).catchError((e) => throw Exception(e));
  }

  /*
  * logout
  *
  * Terminates user session
  */
  Future<void> logout() async {
    storage.delete(key: 'jwt');
    post("/logout", "");
  }

  Future<void> changeUsername({@required String newUsername}) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    await ioClient
        .post(
      url + "/user/operations/changeUsername",
      headers: headers,
      body: newUsername,
    )
        .then((response) {
      this.headers['set-cookie'] = response.headers['set-cookie'];
    }).catchError((e) => throw Exception(e));
  }

  Future<void> changePassword(
      {@required String newPassword, @required String oldPassword}) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    await ioClient
        .post(
          url + "/user/operations//changePassword",
          headers: headers,
          body: jsonEncode(
              {"oldPassword": oldPassword, "newPassword": newPassword}),
        )
        .then((response) {})
        .catchError((e) => throw Exception(e));
  }

  Future<void> changeEmail({@required String newEmail}) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    await ioClient
        .post(
          url + "/user/operations/changeEmail",
          headers: headers,
          body: newEmail,
        )
        .then((response) {})
        .catchError((e) => throw Exception(e));
  }

  Future<void> changeProfile(
      {String newUsername,
      String newEmail,
      String newPassword,
      String oldPassword}) async {
    await changeUsername(newUsername: newUsername);
    if (newPassword != "") {
      await changePassword(newPassword: newPassword, oldPassword: oldPassword);
    }
    await changeEmail(newEmail: newEmail);
  }

  Future<void> forgotPassword(String email) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    await ioClient
        .post(
          url + "/user/operations/forgotMyPassword",
          headers: headers,
          body: jsonEncode({email}),
        )
        .then((response) {})
        .catchError((e) => throw Exception(e));
  }

  Future<dynamic> get(String path) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    return await ioClient
        .get(
      url + path,
      headers: headers,
    )
        .then((response) {
      if (response.statusCode == 200)
        saveCookie(response, this.saveCredentials);
      else if (response.statusCode == 403 && path == '/users/userInfo') {
        deleteCookie();
        // MARANTE o que eu fiz aqui foi apagar a cookie e pedir a info outra vez
        // para correr tudo bem, mas a ideia será verificar quando a app lança se estás logado
        // Fazendo o get para /users/userInfo. Se for o statusCode for 403,
        // Mandas para a página de login. O primeiro pedido não pode mesmo ser para o /events/all
        // Muda isso e depois apaga este request por baixo desta linha (deixando só o deleteCookie acima)
        // return ioClient
        //     .get(
        //   serverDomainName + path,
        //   headers: headers,
        // );

        return response.statusCode;
      }

      if (response.body == null) return {};
      return response.bodyBytes;
    }).catchError((e) {
      return {};
    });
  }

  /*
  * post
  *
  * request: POST
  * @param path: string with endpoint
  * @param body: json
  */
  Future<dynamic> post(String path, body) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    if (!(body is String)) body = jsonEncode(body);

    return await ioClient
        .post(url + path, headers: headers, body: body)
        .then((response) {
      if (response.statusCode == 200)
        saveCookie(response, this.saveCredentials);
      if (response.body.toString() == "") return "";
      return json.decode(utf8.decode(response.bodyBytes));
    }).catchError((e) {
      print(e);
      return {};
    });
  }

  /*
  * post
  *
  * request: POST
  * @param path: string with endpoint
  * @param body: json
  */
  Future<dynamic> delete(String path) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("BACKEND");

    return await ioClient.delete(url + path, headers: headers).then((response) {
      if (response.body.toString() == "") return "";
      return json.decode(response.body);
    }).catchError((e) => throw Exception(e));
  }

  /*
  * webSocketNewEvent
  *
  * request: POST
  * @param eventId: string with event ID
  * @param teams: string with both teams
  * @param modality: string with modality
  */
  Future<void> webSocketNewEvent(
      String eventId, String teams, String modality) async {
    if (ioClient == null) await getIoClient();
    var url = await getEnvURL("WEBSOCKET");

    ioClient.post(url + '/createEvent/' + eventId,
        headers: headers,
        body: json.encode({"teams": teams, "modality": modality}));
  }

  /*
  * getFromWebsocket
  *
  * request: GET
  * @param path: string with endpoint
  */
  Future<dynamic> getFromWebsocket(String path) async {
    var url = await getEnvURL("WEBSOCKET");

    return await ioClient
        .get(
      url + path,
      headers: headers,
    )
        .then((response) {
      print(json.decode(json.encode(response.body)));
      return json.decode(json.encode(response.body));
    }).catchError((e) => throw Exception(e));
  }

  Future<dynamic> getUserInfo() async {
    var info = await serverInterface.get("/users/userInfo");
    if (info == 403) return info;
    Map<dynamic, dynamic> userInfo = json.decode(utf8.decode(info));
    return User.fromJson(userInfo);

    // .then((newUserInfo) => setState(() => userInfo = newUserInfo));
  }

  Future<List<Event>> getAllEvents() async {
    var response = json.decode(utf8.decode(await get("/events/all")));
    print(response[0]);
    if (response == null) return [];
    List<Event> events = [];
    for (int i = 0; i < response.length; i++) {
      events.add(Event.fromJson(response[i]));
    }
    return events;
  }

  String getEventImageId(String sport) {
    return 'https://aws-eagle-stream-app-website-logos.s3.eu-west-3.amazonaws.com/Sports/' +
        sport +
        '.jpg';
  }

  Future<String> createNewEvent(String team1, String team2, DateTime date,
      TimeOfDay time, String sport, String competition) async {
    Map<String, dynamic> newEvent = {
      "sport": sport,
      "teams": [team1, team2],
      "date": {
        "year": date.year.toString(),
        "month": date.month.toString(),
        "day": date.day.toString(),
        "hour": time.hour.toString(),
        "minutes": time.minute.toString(),
      },
      "competition": competition,
    };
    var response = await post("/events/create", newEvent);
    webSocketNewEvent(response.toString(),
        newEvent["teams"][0] + "|" + newEvent["teams"][1], newEvent["sport"]);
    print(response);
    return response.toString();
  }

  void getGameInfoEntry(String eventId, Function callback) {
    getFromWebsocket('/eventInfo/$eventId').then((value) {
      print("value");
      print(value);
      callback(value);
    });
  }

  Future<List<String>> getSports() async {
    var sports = await serverInterface.get('/events/getSports');
    return (json.decode(utf8.decode(sports)) as List).cast<String>().toList();
  }

  void getIsMyEvent(String eventId, Function callback) {
    get("/events/myEvents").then((events) {
      for (var event in events)
        if (event["id"] == eventId) {
          callback();
          break;
        }
    });
  }

  Future<bool> getGameInfo(String eventId, Function callback) async {
    await get('/events/$eventId/eventInfo').then((value) {
      callback(value);
    }).catchError((error) {
      print(error);
      return false;
    });
    return true;
  }

  Future<String> deleteEvent(String eventID) async {
    String response = await delete("/events/{$eventID}/delete");
    return response;
  }

  void startEvent(String eventID) async {
    serverInterface.post("/events/$eventID/startEvent", {});
  }

  Future<List<Event>> getMyEvents() async {
    var response = json.decode(utf8.decode(await get("/events/myEvents")));
    if (response == null) return [];
    List<Event> events = [];
    for (int i = 0; i < response.length; i++) {
      events.add(Event.fromJson(response[i]));
    }
    return events;
  }

  Future<List<String>> getTeamsBySport(String sport) async {
    var response = await get("/teams/bySport/$sport");
    print("teams");
    print(response);
    return (json.decode(utf8.decode(response)) as List).cast<String>().toList();
  }

  Future<dynamic> getTeamsPattern(String pattern) async {
    var response = await get(
        "/teams/${(["", null].contains(pattern) ? "x1c09pp" : pattern)}");
    print("teams");
    print((json.decode(utf8.decode(response))));
    return json.decode(utf8.decode(response));
  }
}
