import 'package:eagle_stream/screens/main/main_screen.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

class MainBottomBar extends StatelessWidget {
  final int selectedTab;
  final Function onSelect;
  final List<AppScreen> pages;

  const MainBottomBar({
    Key key,
    this.selectedTab,
    this.onSelect,
    this.pages,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 60 + MediaQuery.of(context).padding.bottom,
        child: BottomNavigationBar(
          onTap: onSelect,
          currentIndex: selectedTab,
          items: [
            for (int i = 0; i < pages.length; i++)
              BottomNavigationBarItem(
                  title: Text(pages[i].title, style: gotham_bold_small),
                  icon: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Icon(
                      pages[i].icon,
                      size: 30,
                    ),
                  )),
          ],
        ));
  }
}
