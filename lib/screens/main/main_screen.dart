import 'dart:async';
import 'package:eagle_stream/animations/snack_bar_animation.dart';
import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/models/user.dart';
import 'package:eagle_stream/screens/about/about_screen.dart';
import 'package:eagle_stream/screens/events/events_screen.dart';
import 'package:eagle_stream/screens/main/components/main_bottom_bar.dart';
import 'package:eagle_stream/screens/new_event/new_event_screen.dart';
import 'package:eagle_stream/screens/profile/profile_screen.dart';
import 'package:eagle_stream/screens/search/search_screen.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:flutter/material.dart';

class AppScreen {
  final String title;
  final IconData icon;
  const AppScreen({
    @required this.title,
    @required this.icon,
  });
}

List<AppScreen> pages = <AppScreen>[
  AppScreen(
    title: "Eventos",
    icon: Icons.home_outlined,
  ),
  AppScreen(
    title: "Procurar",
    icon: Icons.search,
  ),
  AppScreen(
    title: "Sobre",
    icon: Icons.help,
  ),
  AppScreen(
    title: "Perfil",
    icon: Icons.account_circle_rounded,
  ),
];

List<AppScreen> pagesAdmin = <AppScreen>[
  AppScreen(
    title: "Eventos",
    icon: Icons.home_outlined,
  ),
  AppScreen(
    title: "Procurar",
    icon: Icons.search,
  ),
  AppScreen(
    title: "Criar Evento",
    icon: Icons.add_circle_outline,
  ),
  AppScreen(
    title: "Sobre",
    icon: Icons.help,
  ),
  AppScreen(
    title: "Perfil",
    icon: Icons.account_circle_rounded,
  ),
];

class MainScreen extends StatefulWidget {
  final Function onLogout;

  MainScreen({
    Key key,
    @required this.onLogout,
  }) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int selectedTab = 0;
  List<Event> events;

  User user;

  int hasData;

  Timer timer;

  final ServerInterface serverInterface = ServerInterface.getInstance();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void onTabTapped(int index) {
    setState(() {
      selectedTab = index;
      _tabController.animateTo(selectedTab, duration: Duration(seconds: 1));
    });
  }

  void getData() {
    serverInterface.getAllEvents().then((fetchedEvents) {
      setState(() {
        events = fetchedEvents;
        hasData++;
      });
      print(hasData);
    });
    serverInterface.getUserInfo().then((fetchedUser) {
      print("user");
      print(fetchedUser.name);
      setState(() {
        user = fetchedUser;
        hasData++;
        _tabController = TabController(
            length: user.isAdmin ? pagesAdmin.length : pages.length,
            //length: pagesAdmin.length,
            vsync: this);
      });
      print(hasData);
    });
  }

  void updateUser() {
    serverInterface.getUserInfo().then((fetchedUser) {
      setState(() {
        user = fetchedUser;
      });
    });
  }

  void updateEvents() {
    serverInterface.getAllEvents().then((fetchedEvents) {
      for (int i = 0; i < fetchedEvents.length; i++) {
        bool found = false;
        for (int j = 0; j < events.length; j++) {
          if (events[j].id == fetchedEvents[i].id) {
            found = true;
          }
        }
        if (!found) {
          events.add(fetchedEvents[i]);
          setState(() {});
        }
      }

      for (int i = 0; i < events.length; i++) {
        bool found = false;
        for (int j = 0; j < fetchedEvents.length; j++) {
          if (events[j].id == fetchedEvents[i].id) {
            found = true;
          }
        }
        if (!found) {
          events.remove(fetchedEvents[i]);
          setState(() {});
        }
      }
    });
  }

  void onCreateEvent() {
    onTabTapped(0);
    updateEvents();
    ShowSnackBarAnimation.showSnackBar(
        _scaffoldKey, "Evento criado com sucesso", false, 2);
  }

  @override
  void initState() {
    super.initState();
    hasData = 0;
    getData();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return hasData == 2
        ? Scaffold(
            key: _scaffoldKey,
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: [
                EventsScreen(
                  events: events ?? [],
                  mainEvent: events != null ? events[0] : null,
                ),
                Search(events: events),
                if (user.isAdmin)
                  NewEvent(
                    onCreateEvent: onCreateEvent,
                  ),
                About(),
                Profile(
                    onRefreshProfile: updateUser,
                    user: user,
                    onLogout: widget.onLogout),
              ],
            ),
            bottomNavigationBar: MainBottomBar(
              pages: user.isAdmin ? pagesAdmin : pages,
              selectedTab: selectedTab,
              onSelect: onTabTapped,
            ),
          )
        : Center(child: Container(child: CircularProgressIndicator()));
  }
}
