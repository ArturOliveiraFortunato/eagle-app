import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/material.dart';

Widget checkbox({@required bool value, @required Function onChanged}) {
  return Container(
      height: 20,
      width: 20,
      decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(width: 3.0, color: white),
            left: BorderSide(width: 3.0, color: white),
            right: BorderSide(width: 3.0, color: white),
            bottom: BorderSide(width: 3.0, color: white),
          ),
          borderRadius: BorderRadius.all(Radius.circular(2))),
      child: Checkbox(
        value: value,
        activeColor: salmon,
        onChanged: onChanged,
      ));
}
