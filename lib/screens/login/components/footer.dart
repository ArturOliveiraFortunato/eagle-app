import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/cupertino.dart';

Widget footer({
  @required String text1,
  @required Function onClick1,
  @required String text2,
  @required Function onClick2,
}) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 20.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: onClick1,
          child: Text(text1, style: gotham_bold_small),
        ),
        Text("  |  ", style: gotham_bold_small),
        GestureDetector(
          onTap: onClick2,
          child: Text(text2, style: gotham_bold_small.copyWith(color: red)),
        ),
      ],
    ),
  );
}
