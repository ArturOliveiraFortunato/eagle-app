import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget title({@required String title}) {
  return Text(
    title,
    style: gotham_medium_big,
  );
}
