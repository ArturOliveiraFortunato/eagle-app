import 'package:eagle_stream/screens/login/components/footer.dart';
import 'package:eagle_stream/screens/login/components/title.dart';
import 'package:eagle_stream/screens/login/forgot_password.dart';
import 'package:eagle_stream/screens/login/login_screen.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/custom_popup.dart';
import 'package:eagle_stream/widgets/custom_text_field.dart';
import 'package:eagle_stream/widgets/logo.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

// ignore: must_be_immutable
class CreateAccountScreen extends StatefulWidget {
  Function onCreateAccount;
  final Map<String, Function> handler;
  final Map<String, bool> status;

  CreateAccountScreen({
    Key key,
    @required this.handler,
    @required this.status,
  }) : super(key: key) {
    this.onCreateAccount = handler["create_account"];
  }
  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  bool failed = false;
  String username = "";
  String email = "";
  String password = "";
  String confirmPassword = "";

  void onConfirm() {
    if (password == confirmPassword) {
      widget.onCreateAccount(
          username: username, password: password, email: email);
    } else {
      setState(() {
        failed = true;
      });
    }
    customPopup(
        context, "Ative a sua conta abrindo o link enviado para o seu email.", [
      DialogButton(
        color: salmon,
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text(
          "ENTENDI",
          style: gotham_book_medium,
        ),
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 80),
                logo(height: 150),
                SizedBox(height: 80),
                title(title: "Criar uma conta"),
                SizedBox(height: 20),
                failed
                    ? Text(
                        "Passwords must match",
                        style: gotham_medium_small.copyWith(color: salmon),
                      )
                    : Container(),
                SizedBox(height: 10),
                customTextFiled(
                  hint: "NOME",
                  hideText: false,
                  onChanged: (text) {
                    setState(() {
                      username = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customTextFiled(
                  hint: "EMAIL",
                  hideText: false,
                  onChanged: (text) {
                    setState(() {
                      email = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customTextFiled(
                  hint: "PALAVRA-PASSE",
                  hideText: true,
                  onChanged: (text) {
                    setState(() {
                      password = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customTextFiled(
                  hint: "CONFIRMAR PALAVRA-PASSE",
                  hideText: true,
                  onChanged: (text) {
                    setState(() {
                      confirmPassword = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customButton(text: "CRIAR CONTA", onClick: onConfirm),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: footer(
          text1: "Esqueci-me da palavra-passe",
          onClick1: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ForgotPassword(
                            handler: widget.handler, status: widget.status)))
              },
          text2: "Iniciar sessão",
          onClick2: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoginScreen(
                            handler: widget.handler, status: widget.status)))
              }),
    );
  }
}
