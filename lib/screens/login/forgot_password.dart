import 'package:eagle_stream/screens/login/components/footer.dart';
import 'package:eagle_stream/screens/login/components/title.dart';
import 'package:eagle_stream/screens/login/create_account_screen.dart';
import 'package:eagle_stream/widgets/custom_text_field.dart';
import 'package:eagle_stream/widgets/logo.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ForgotPassword extends StatefulWidget {
  Function onForgetPassword;
  final Map<String, Function> handler;
  final Map<String, bool> status;

  ForgotPassword({
    Key key,
    @required this.handler,
    @required this.status,
  }) : super(key: key) {
    this.onForgetPassword = handler["forgot_password"];
  }

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  String email = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 80),
                logo(height: 150),
                SizedBox(height: 80),
                title(title: "Esqueci-me da password"),
                SizedBox(height: 20),
                customTextFiled(
                  hint: "EMAIL",
                  hideText: false,
                  onChanged: (text) {
                    setState(() {
                      email = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customButton(
                  text: "ENVIAR EMAIL",
                  onClick: () => widget.onForgetPassword(email),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: footer(
          text1: "Iniciar sessão",
          onClick1: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CreateAccountScreen(
                              handler: widget.handler,
                              status: widget.status,
                            )))
              },
          text2: "Criar conta",
          onClick2: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CreateAccountScreen(
                              handler: widget.handler,
                              status: widget.status,
                            )))
              }),
    );
  }
}
