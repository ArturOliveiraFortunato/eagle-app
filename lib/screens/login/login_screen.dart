import 'package:eagle_stream/screens/login/components/checkbox.dart';
import 'package:eagle_stream/screens/login/components/footer.dart';
import 'package:eagle_stream/screens/login/components/title.dart';
import 'package:eagle_stream/screens/login/create_account_screen.dart';
import 'package:eagle_stream/screens/login/forgot_password.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/custom_text_field.dart';
import 'package:eagle_stream/widgets/logo.dart';
import 'package:eagle_stream/widgets/play_button.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {
  Function onLogin;
  Function onToggle;
  Function onFail;
  final Map<String, Function> handler;
  final Map<String, bool> status;

  LoginScreen({
    Key key,
    @required this.handler,
    @required this.status,
  }) : super(key: key) {
    this.onLogin = handler["login"];
    this.onToggle = handler["toggle_stay_logged"];
  }

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String username = "";
  String password = "";

  final ServerInterface serverInterface = ServerInterface.getInstance();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 80),
                logo(height: 150),
                SizedBox(height: 80),
                title(title: "Bem-vindo"),
                SizedBox(height: 20),
                widget.status['failedLogin']
                    ? Text(
                        "USERNAME OU PALAVRA-PASS INCORRETO/A",
                        style: gotham_medium_small.copyWith(color: salmon),
                      )
                    : Container(),
                SizedBox(height: 10),
                customTextFiled(
                  hint: "USERNAME",
                  hideText: false,
                  onChanged: (text) {
                    setState(() {
                      username = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                customTextFiled(
                  hint: "PASSWORD",
                  hideText: true,
                  onChanged: (text) {
                    setState(() {
                      password = text;
                    });
                  },
                ),
                SizedBox(height: 20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        checkbox(
                            value: widget.status['stayLogged'],
                            onChanged: (value) {
                              widget.onToggle(value);
                            }),
                        SizedBox(
                          width: 20,
                        ),
                        Text("Manter-me conectado",
                            style: gotham_book_small.copyWith(
                                color: Colors.white)),
                      ],
                    ),
                    Expanded(child: SizedBox()),
                    playButton(onPressed: () {
                      widget.onLogin(username: username, password: password);
                    })
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: footer(
          text1: "Esqueci-me da palavra-passe",
          onClick1: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ForgotPassword(handler: widget.handler, status: widget.status)))
              },
          text2: "Criar conta",
          onClick2: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CreateAccountScreen(handler: widget.handler, status: widget.status)))
              }),
    );
  }
}
