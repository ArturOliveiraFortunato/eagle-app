import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EventDatePicker extends StatefulWidget {
  final Function onChangeDate;
  final Function onChangeTime;
  final DateTime date;
  final TimeOfDay time;
  final int num;

  EventDatePicker({
    Key key,
    @required this.onChangeDate,
    @required this.onChangeTime,
    @required this.date,
    @required this.time,
    @required this.num,
  }) : super(key: key);

  @override
  _EventDatePickerState createState() => _EventDatePickerState();
}

class _EventDatePickerState extends State<EventDatePicker> {
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  @override
  initState() {
    super.initState();
    _dateController.text = DateFormat.yMd().format(widget.date);
    _timeController.text =
        widget.time.hour.toString() + " : " + widget.time.minute.toString();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: widget.date,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              primaryColor: salmon,
              accentColor: white,
              colorScheme: ColorScheme.light(primary: salmon),
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        widget.onChangeDate(picked);
        _dateController.text = DateFormat.yMd().format(widget.date);
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: widget.time,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              primaryColor: salmon,
              accentColor: white,
              colorScheme: ColorScheme.light(primary: salmon),
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        widget.onChangeTime(picked);

        _timeController.text =
            widget.time.hour.toString() + " : " + widget.time.minute.toString();
      });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    color: salmon,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                      child: Text(widget.num.toString(),
                          style: gotham_book_medium)),
                ),
                SizedBox(width: 15),
                Text("DATA E HORA", style: gotham_book_medium),
              ],
            ),
            SizedBox(height: 20),
            Row(
              children: [
                InkWell(
                  onTap: () => _selectDate(context),
                  child: Container(
                    height: 30,
                    width: 90,
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(bottom: 18),
                      ),
                      style: gotham_book_medium.copyWith(color: grey),
                      textAlign: TextAlign.center,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      controller: _dateController,
                    ),
                  ),
                ),
                SizedBox(width: 30),
                InkWell(
                  onTap: () => _selectTime(context),
                  child: Container(
                    height: 30,
                    width: 90,
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(bottom: 18),
                      ),
                      style: gotham_book_medium.copyWith(color: grey),
                      textAlign: TextAlign.center,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      controller: _timeController,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(width: 30),
      ],
    );
  }
}
