import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:eagle_stream/widgets/dropdown.dart';
import 'package:eagle_stream/widgets/textfield_autocomplete.dart';
import 'package:eagle_stream/widgets/textfield_autocomplete2.dart';
import 'package:flutter/material.dart';

class NewEventSection extends StatefulWidget {
  final int num;
  final String sectionName;
  final Map<String, Function> handler;
  final String value1;
  final String value2;
  final List<String> options;

  NewEventSection({
    Key key,
    @required this.num,
    @required this.sectionName,
    @required this.handler,
    @required this.value1,
    this.value2,
    this.options,
  }) : super(key: key);

  @override
  _NewEventSectionState createState() => _NewEventSectionState();
}

class _NewEventSectionState extends State<NewEventSection> {
  @override
  Widget build(BuildContext context) {
    print(widget.options);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              height: 20,
              width: 20,
              decoration: BoxDecoration(
                color: salmon,
                shape: BoxShape.circle,
              ),
              child: Center(
                  child:
                      Text(widget.num.toString(), style: gotham_book_medium)),
            ),
            SizedBox(width: 15),
            Text(widget.sectionName, style: gotham_book_medium),
          ],
        ),
        SizedBox(height: 10),
        widget.sectionName == "EQUIPAS / ATLETAS"
            ? Column(
                children: [
                  SizedBox(height: 30),
                  TextFieldAutoComplete2(
                      hint: "EQUIPA 1",
                      value: widget.value1,
                      suggestions: widget.options,
                      onChange: (value) => widget.handler["team1"](value)),
                  SizedBox(height: 30),
                  TextFieldAutoComplete2(
                      hint: "EQUIPA 2",
                      value: widget.value2,
                      suggestions: widget.options,
                      onChange: (value) => widget.handler["team2"](value)),
                  SizedBox(height: 30),
                ],
              )
            // widget.sectionName == "EQUIPAS / ATLETAS"
            //     ? Row(
            //         children: [
            //           dropdown(
            //               hint: "EQUIPA 1",
            //               value: widget.value1,
            //               items: widget.options,
            //               onChanged: (value) => widget.handler["team1"](value)),
            //           SizedBox(width: 30),
            //           dropdown(
            //               hint: "EQUIPA 2",
            //               value: widget.value2,
            //               items: widget.options,
            //               onChanged: (value) => widget.handler["team2"](value)),
            //         ],
            //       )
            : dropdown(
                hint: widget.sectionName,
                value: widget.value1,
                items: widget.options,
                onChanged: (value) => widget
                    .handler[sectionTranslator[widget.sectionName]](value)),
      ],
    );
  }
}

Map<String, String> sectionTranslator = {
  "MODALIDADE": "modality",
  "COMPETIÇÃO": "competition",
  "ESCALÃO": "echelon",
};
