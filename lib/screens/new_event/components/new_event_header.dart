import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

class NewEventHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: black,
            border: Border.all(color: salmon),
          ),
          child: Center(
            child: Container(
              height: 18,
              width: 18,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: salmon,
              ),
            ),
          ),
        ),
        SizedBox(width: 20),
        Text("Criar um evento", style: gotham_bold_big),
      ],
    );
  }
}
