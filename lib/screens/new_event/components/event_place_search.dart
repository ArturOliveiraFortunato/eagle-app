import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

class EventPlaceSearch extends StatelessWidget {
  final int num;

  EventPlaceSearch({
    Key key,
    @required this.num,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 20,
              width: 20,
              decoration: BoxDecoration(
                color: salmon,
                shape: BoxShape.circle,
              ),
              child: Center(
                  child: Text(num.toString(), style: gotham_book_medium)),
            ),
            SizedBox(width: 15),
            Text("LOCAL", style: gotham_book_medium),
          ],
        ),
        SizedBox(height: 20),
        // SearchMapPlaceWidget(apiKey: 'AIzaSyCgFVUMun7ZCjeYW7A_xBpY6WDM2M0cY5w'),
      ],
    );
  }
}
