import 'dart:async';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:eagle_stream/animations/snack_bar_animation.dart';
import 'package:eagle_stream/screens/new_event/components/event_date_picker.dart';
import 'package:eagle_stream/screens/new_event/components/event_place_search.dart';
import 'package:eagle_stream/screens/new_event/components/new_event_header.dart';
import 'package:eagle_stream/screens/new_event/components/new_event_section.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class NewEvent extends StatefulWidget {
  final Function onCreateEvent;

  NewEvent({Key key, @required this.onCreateEvent}) : super(key: key);

  @override
  _NewEventState createState() => _NewEventState();
}

class _NewEventState extends State<NewEvent> {
  String _team1;
  String _team2;
  String _modality;
  String _competition;
  List<String> modalities;
  List<String> teams;

  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final ServerInterface serverInterface = ServerInterface.getInstance();

  Map<String, Function> onChangeHandler;

  @override
  void initState() {
    super.initState();
    teams = [""];
    modalities = [""];
    serverInterface.getSports().then((sports) {
      for (int i = 0; i < sports.length; i++) {
        print(sports[i] + " " + sports[i].length.toString());
      }
      setState(() {
        modalities = sports;
      });
      serverInterface
          .getTeamsBySport(sports[0])
          .then((teamNames) => setState(() {
                teams = teamNames;
              }));
    });
    onChangeHandler = {
      "team1": (String value) {
        setState(() {
          _team1 = value;
        });
      },
      "team2": (String value) {
        setState(() {
          _team2 = value;
        });
      },
      "modality": (String value) {
        setState(() {
          _modality = value;
        });
        serverInterface.getTeamsBySport(value).then((teamNames) => setState(() {
              teams = teamNames;
            }));
      },
      "competition": (String value) {
        setState(() {
          _competition = value;
        });
      },
      "echelon": (String value) {
        setState(() {});
      },
    };
  }

  onChangeDate(DateTime date) {
    setState(() {
      _date = date;
    });
  }

  onChangeTime(TimeOfDay time) {
    setState(() {
      _time = time;
    });
  }

  @override
  Widget build(BuildContext context) {
    int num = 1;
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                NewEventHeader(),
                SizedBox(height: 50),
                NewEventSection(
                    options: modalities,
                    num: num++,
                    sectionName: "MODALIDADE",
                    handler: onChangeHandler,
                    value1: _modality),
                SizedBox(height: 25),
                // NewEventSection(
                //     num: num++,
                //     sectionName: "COMPETIÇÃO",
                //     handler: onChangeHandler,
                //     value1: _competition),
                // SizedBox(height: 25),
                // NewEventSection(num: 2, sectionName: "ESCALÃO"),
                // SizedBox(height: 25),

                NewEventSection(
                    options: teams,
                    num: num++,
                    sectionName: "EQUIPAS / ATLETAS",
                    handler: onChangeHandler,
                    value1: _team1,
                    value2: _team2),
                SizedBox(height: 25),
                EventDatePicker(
                    num: num++,
                    onChangeDate: onChangeDate,
                    onChangeTime: onChangeTime,
                    date: _date,
                    time: _time),
                SizedBox(height: 25),
                //EventPlaceSearch(num: num++),
                SizedBox(height: 25),
                customButton(
                    icon: Icons.add,
                    text: "CRIAR EVENTO",
                    onClick:
                        (_team1 != null && _team2 != null && _modality != null)
                            ? () => serverInterface
                                    .createNewEvent(
                                        _team1,
                                        _team2,
                                        _date,
                                        _time,
                                        _modality.toLowerCase(),
                                        "_competition")
                                    .then((response) {
                                  print("response");
                                  print(response);
                                  if (response != null) {
                                    widget.onCreateEvent();
                                  } else {
                                    ShowSnackBarAnimation.showSnackBar(
                                        _scaffoldKey,
                                        "Ocorreu um erro. Tente novamente.",
                                        true,
                                        2);
                                  }
                                })
                            : () {
                                ShowSnackBarAnimation.showSnackBar(
                                    _scaffoldKey,
                                    "Ocorreu um erro. Tente novamente.",
                                    true,
                                    2);
                              }), //TODO error message should appear
              ],
            ),
          ),
        ),
      ),
    );
  }
}
