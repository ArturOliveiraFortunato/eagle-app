import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/team_emblem.dart';
import 'package:flutter/material.dart';

class TeamMember {
  String name;
  String role;
  String description;
  AssetImage image;

  TeamMember({
    @required this.name,
    @required this.role,
    @required this.description,
    @required this.image,
  });
}

Widget teamMember(
    {@required TeamMember member, @required BuildContext context}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      //width: MediaQuery.of(context).size.width / 2,
      child: Column(
        children: [
          TeamEmblem(size: 120, image: member.image),
          SizedBox(height: 10),
          Text(
            member.name,
            style: gotham_medium_big,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(height: 10),
          Text(
            member.role.toUpperCase(),
            style: gotham_medium_small,
          ),
        ],
      ),
    ),
  );
}

Widget aboutUs(BuildContext context) {
  List<TeamMember> teamMembers = [
    TeamMember(
        name: "Alexandre",
        role: "Operations Manager",
        description:
            "Uma pessoa motivada sempre pronta para um novo desafio. Adora front end e bolos.",
        image: AssetImage('assets/images/members/Alexandre.jpg')),
    TeamMember(
        name: "Artur",
        role: "Product Owner",
        description:
            "Uma pessoa motivada sempre pronta para um novo desafio. Adora front end e bolos.",
        image: AssetImage('assets/images/members/Artur.jpg')),
    TeamMember(
        name: "Filipe",
        role: "Software Engineer",
        description:
            "Uma pessoa motivada sempre pronta para um novo desafio. Adora front end e bolos.",
        image: AssetImage('assets/images/members/Filipe.jpg')),
    TeamMember(
        name: "Francisco",
        role: "Sales Manager",
        description:
            "Uma pessoa motivada sempre pronta para um novo desafio. Adora front end e bolos.",
        image: AssetImage('assets/images/members/Francisco.jpg')),
  ];

  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 30),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Quem somos",
            style: gotham_medium_big,
          ),
        ),
      ),
      GridView.count(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        childAspectRatio: 0.9,
        crossAxisCount: 2,
        shrinkWrap: true,
        children: List.generate(
            teamMembers.length,
            (index) =>
                teamMember(member: teamMembers[index], context: context)),
      )
    ],
  );
}
