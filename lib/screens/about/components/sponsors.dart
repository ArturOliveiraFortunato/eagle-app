import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/team_emblem.dart';
import 'package:flutter/material.dart';

Widget sponsors() {
  List<AssetImage> sponsors = [
    AssetImage('assets/images/sponsors/adesl.jpeg'),
    AssetImage('assets/images/sponsors/iapmei.jpeg'),
    AssetImage('assets/images/sponsors/incd.jpeg'),
    AssetImage('assets/images/sponsors/startup-tn.jpeg'),
  ];
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 30),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Sponsors",
            style: gotham_medium_big,
          ),
        ),
      ),
      Container(
        height: 150,
        child: Stack(
          children: [
            Align(
              child: Container(
                height: 1,
                color: white,
                alignment: Alignment.center,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (int i = 0; i < sponsors.length; i++)
                    TeamEmblem(image: sponsors[i], size: 70)
                ],
              ),
            )
          ],
        ),
      )
    ],
  );
}
