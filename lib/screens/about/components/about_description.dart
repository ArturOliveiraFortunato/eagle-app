import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/logo.dart';
import 'package:flutter/material.dart';

Widget aboutDescription(BuildContext context) {
  return Stack(
    children: [
      Container(
        child: Image.asset(
          "assets/images/interface-photo.png",
          height: 520.0,
          fit: BoxFit.cover,
        ),
      ),
      Container(
        height: 520.0,
        decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.grey.withOpacity(0.0),
                Colors.black,
              ],
            )),
      ),
      Positioned(
        top: 40,
        left: 30,
        child: logo(
          height: 60,
        ),
      ),
      Positioned(
        bottom: 30,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only(right: 30, left: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Sobre o Eagle Stream",
                style: gotham_medium_big,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "O EagleStream é uma plataforma de transmissão de eventos desportivos que parte de uma óptica de captação de vídeo por parte dos espetadores. Devido à situação atual estamos limitados, mas o desporto não pode parar, por isso podes contar connosco para ver e rever jogos das tuas modalidades preferidas! Por agora, estamos focados no desporto universitário, mas brevemente a questão vai ser que jogos é que não transmitimos. Agradecemos qualquer tipo de feedback, por isso se tiveres sugestões entra em contacto com qualquer membro da equipa (já por baixo) ou com as nossas redes sociais.",
                style: gotham_book_medium.copyWith(height: 1.5),
              ),
            ],
          ),
        ),
      )
    ],
  );
}
