import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget _policyTopic(String text) {
  return Padding(
    padding: const EdgeInsets.only(top: 20),
    child: Text(
      text,
      style: gotham_book_medium,
      maxLines: 1,
      softWrap: false,
      overflow: TextOverflow.fade,
    ),
  );
}

Widget _policySubTopic(String text) {
  return Padding(
    padding: const EdgeInsets.only(top: 10),
    child: Text(text, style: gotham_book_small),
  );
}

Widget privacyPolicy(BuildContext context) {
  return Scaffold(
    body: SafeArea(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 30.0),
                child: customButton(
                    text: "VOLTAR",
                    onClick: () => Navigator.pop(context),
                    icon: Icons.arrow_back_rounded),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 20, right: 30),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Política de Privacidade",
                        style: gotham_medium_big,
                      ),
                    ),
                    for (int i = 0; i < policyTopics.length; i++)
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                _policyTopic((i + 1).toString() +
                                    ". " +
                                    policyTopics[i]),
                              ],
                            ),
                            for (int j = 0;
                                j < policySubTopics[policyTopics[i]].length;
                                j++)
                              _policySubTopic((i + 1).toString() +
                                  "." +
                                  (j + 1).toString() +
                                  ". " +
                                  policySubTopics[policyTopics[i]][j])
                          ]),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
