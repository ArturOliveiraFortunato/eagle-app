import 'package:eagle_stream/screens/about/components/about_description.dart';
import 'package:eagle_stream/screens/about/components/about_us.dart';
import 'package:eagle_stream/screens/about/components/privacy_policy.dart';
import 'package:eagle_stream/screens/about/components/sponsors.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          aboutDescription(context),
          sponsors(),
          aboutUs(context),
          customButton(
              text: 'POLÍTICA DE PRIVACIDADE',
              icon: Icons.policy_outlined,
              onClick: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => privacyPolicy(context)))),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
