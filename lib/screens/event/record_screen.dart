import 'dart:async';
import 'dart:io';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/animations/snack_bar_animation.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:camera_with_rtmp/camera.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class RecordScreen extends StatefulWidget {
  final String eventId;

  RecordScreen({Key key, this.eventId}) : super(key: key);

  @override
  _RecordScreen createState() {
    return _RecordScreen();
  }
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _RecordScreen extends State<RecordScreen> with WidgetsBindingObserver {
  List<CameraDescription> cameras = [];
  CameraController controller;
  String imagePath;
  String videoPath;
  String url;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool enableAudio = true;
  bool useOpenGL = true;
  TextEditingController _textFieldController =
      TextEditingController(text: "rtmp://" + DotEnv().env['VIDEO'] + ":1935/live/");
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Timer _timer;

  ServerInterface serverInterface = ServerInterface.getInstance();
  static const String ONGOING = "ONGOING";

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      onNewCameraSelected(cameras[0]);
    });
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
    ]);

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    stopVideoStreaming();
    controller.dispose();
    videoController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
      if (_timer != null) {
        _timer.cancel();
        _timer = null;
      }
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      /*appBar: AppBar(
                backgroundColor: Colors.black,
            ),*/
      body: Stack(
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.all(1.0),
              child: Center(
                child: _cameraPreviewWidget(),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.black,
              border: Border.all(
                color: controller != null && controller.value.isRecordingVideo
                    ? controller.value.isStreamingVideoRtmp
                        ? Colors.redAccent
                        : Colors.orangeAccent
                    : controller != null &&
                            controller.value.isStreamingVideoRtmp
                        ? Colors.blueAccent
                        : Colors.grey,
                width: 3.0,
              ),
            ),
          ),
          Positioned(
            child: Center(
              child: _captureControlRowWidget(),
            ),
            bottom: 0,
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _thumbnailWidget(),
              ],
            ),
          ),
          Positioned(
            top: 10,
            left: 20,
            child: Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 30.0),
              child: customButton(
                  text: "VOLTAR",
                  onClick: () => Navigator.pop(context),
                  icon: Icons.arrow_back_rounded),
            ),
          ),
        ],
      ),
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  /// Display the thumbnail of the captured image or video.
  Widget _thumbnailWidget() {
    return Expanded(
      child: Align(
        alignment: Alignment.centerRight,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            videoController == null && imagePath == null
                ? Container()
                : SizedBox(
                    child: (videoController == null)
                        ? Image.file(File(imagePath))
                        : Container(
                            child: Center(
                              child: AspectRatio(
                                  aspectRatio:
                                      videoController.value.size != null
                                          ? videoController.value.aspectRatio
                                          : 1.0,
                                  child: VideoPlayer(videoController)),
                            ),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.pink)),
                          ),
                    width: 64.0,
                    height: 64.0,
                  ),
          ],
        ),
      ),
    );
  }

  /// Display the control bar with buttons to take pictures and record videos.
  Widget _captureControlRowWidget() {
    var isRecording = controller != null &&
        controller.value.isInitialized &&
        !controller.value.isStreamingVideoRtmp;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        isRecording
            ? Container(
                width: MediaQuery.of(context).size.width,
                child: IconButton(
                    icon: const Icon(Icons.adjust),
                    color: Colors.red,
                    onPressed: onVideoStreamingButtonPressed),
              )
            : Container(
                width: MediaQuery.of(context).size.width,
                child: IconButton(
                    icon: const Icon(Icons.stop),
                    color: Colors.red,
                    onPressed: onStopButtonPressed),
              ),
        /*IconButton(
                    icon: const Icon(Icons.adjust),
                    color: Colors.red,
                    onPressed: controller != null &&
                        controller.value.isInitialized &&
                        !controller.value.isStreamingVideoRtmp
                        ? onVideoStreamingButtonPressed
                        : null,
                ),
                IconButton(
                    icon: const Icon(Icons.stop),
                    color: Colors.red,
                    onPressed: controller != null &&
                        controller.value.isInitialized &&
                        (controller.value.isRecordingVideo ||
                            controller.value.isStreamingVideoRtmp)
                        ? onStopButtonPressed
                        : null,
                )*/
      ],
    );
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message, bool error) {
    ShowSnackBarAnimation.showSnackBar(_scaffoldKey, message, error, 1);
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.high, // low, medium, high, veryHigh, ultraHigh, max
      enableAudio: enableAudio,
      androidUseOpenGL: useOpenGL,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar(
            'Camera error ${controller.value.errorDescription}', true);
        if (_timer != null) {
          _timer.cancel();
          _timer = null;
        }
        Wakelock.disable();
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onVideoStreamingButtonPressed() {
    serverInterface.post(
        "/events/" + widget.eventId + "/updateEventState", ONGOING);
    startVideoStreaming().then((String url) {
      if (mounted) setState(() {});
      if (url != null) showInSnackBar('Streaming video to $url', false);
      Wakelock.enable();
    });
  }

  void onStopButtonPressed() {
    if (this.controller.value.isStreamingVideoRtmp) {
      stopVideoStreaming().then((_) {
        if (mounted) setState(() {});
      });
    }
    Wakelock.disable();
  }

  void onStopStreamingButtonPressed() {
    stopVideoStreaming().then((_) {
      if (mounted) setState(() {});
    });
  }

  /*Future<String> _getUrl() async {
        // Open up a dialog for the url
        String result = _textFieldController.text + widget.eventId;

        return await showDialog(
            context: context,
            builder: (context) {
                return AlertDialog(
                    title: Text('Url to Stream to'),
                    content: TextField(
                        controller: _textFieldController,
                        decoration: InputDecoration(hintText: "Url to Stream to"),
                        onChanged: (String str) => result = str,
                    ),
                    actions: <Widget>[
                        new FlatButton(
                            child: new Text(
                                MaterialLocalizations.of(context).cancelButtonLabel),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        ),
                        FlatButton(
                            child: Text(MaterialLocalizations.of(context).okButtonLabel),
                            onPressed: () {
                                Navigator.pop(context, result);
                            },
                        )
                    ],
                );
            });
    }*/

  Future<String> startVideoStreaming() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.', false);
      return null;
    }

    if (controller.value.isStreamingVideoRtmp) {
      return null;
    }

    String myUrl = _textFieldController.text + widget.eventId;
    try {
      if (_timer != null) {
        _timer.cancel();
        _timer = null;
      }
      url = myUrl;
      await controller.startVideoStreaming(url);
      _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
        var stats = await controller.getStreamStatistics();
      });
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return url;
  }

  Future<void> stopVideoStreaming() async {
    if (!controller.value.isStreamingVideoRtmp) {
      return null;
    }

    try {
      await controller.stopVideoStreaming();
      if (_timer != null) {
        _timer.cancel();
        _timer = null;
      }
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}', true);
  }
}
