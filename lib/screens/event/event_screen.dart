import 'dart:convert';

import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/models/statistics.dart';
import 'package:eagle_stream/screens/event/components/event_scoreboard.dart';
import 'package:eagle_stream/screens/event/components/game_controls.dart';
import 'package:eagle_stream/screens/event/components/rtmp_player.dart';
import 'package:eagle_stream/screens/event/components/stats.dart';
import 'package:eagle_stream/screens/event/record_screen.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/services/websocketInterface.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class EventScreen extends StatefulWidget {
  final Event event;

  EventScreen({
    Key key,
    @required this.event,
  }) : super(key: key);

  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  bool controlling = false;
  bool fullscreen = false;
  String dropdownValue1;
  String dropdownValue2;

  WebsocketInterface websocketInterface = WebsocketInterface();
  ServerInterface serverInterface = ServerInterface.getInstance();

  Map<String, Function> handler = Map<String, Function>();

  void toggleControls() {
    setState(() {
      controlling = !controlling;
    });
  }

  void onChangeDropdown1(String value) {
    setState(() {
      dropdownValue1 = value;
    });
  }

  void onChangeDropdown2(String value) {
    setState(() {
      dropdownValue2 = value;
    });
  }

  void onToggleFullscreen() {
    setState(() {
      fullscreen = !fullscreen;
    });
  }

  void onUpdateStat(String stat, int value1, int value2) {
    widget.event.updateStat(stat, value1, value2);
    setState(() {});
  }

  void sendUpdatedStat(int value, String statName, String team) {
    Map<String, dynamic> message = {
      "type": "UPDATE_STATE",
    };
    Map<String, dynamic> content = Map<String, dynamic>();
    content["stat"] = statName;
    content["team"] = team;
    content["newValue"] = value;
    message["content"] = content;
    websocketInterface.changeEventState(message, widget.event.id);
  }

  int getIndexT1(dynamic stat) {
    return stat.keys.toList()[0] == widget.event.team1.name ? 0 : 1;
  }

  void updateStatistics(Map<String, dynamic> content) {
    List<String> statsNames = [];

    List<dynamic> statsTeam1 = [];
    List<dynamic> statsTeam2 = [];

    if (content["setResults"] != null) {
      Map<String, dynamic> sets = content.remove('setResults');
      for (int i = 0; i < sets.length; i++) {
        statsNames.add(i.toString());
        var values = sets[i.toString()].values.toList();
        statsTeam1.add(values[getIndexT1(sets[i.toString()])]);
        statsTeam2.add(values[1 - getIndexT1(sets[i.toString()])]);
      }
    }
    if (content["advantage"] != null) {
      var adv = content.remove("advantage");
      statsNames.add("advantage");
      print(content.values.toList()[0].keys.toList()[0]);
      statsTeam1.add(adv == widget.event.team1.name ? 1 : 0);
      statsTeam2.add(adv == widget.event.team2.name ? 1 : 0);
    }

    statsNames += content.keys.toList();
    statsTeam1 += content.values
        .toList()
        .map((e) => e.values.toList()[getIndexT1(e)])
        .toList();
    statsTeam2 += content.values
        .toList()
        .map((e) => e.values.toList()[1 - getIndexT1(e)])
        .toList();
    String team1Name = widget.event.team1.name;
    String team2Name = widget.event.team2.name;
    Statistics newStats = Statistics(
        statsNames: statsNames,
        statsTeam1: statsTeam1,
        statsTeam2: statsTeam2,
        team1Name: team1Name,
        team2Name: team2Name);

    print(statsNames);

    setState(() {
      widget.event.updateStatistics(newStats);
    });
  }

  @override
  void initState() {
    super.initState();
    websocketInterface.openConnection(widget.event.id, updateStatistics);
    serverInterface.getGameInfoEntry(widget.event.id, (response) {
      print(response);
      updateStatistics(jsonDecode(response)["content"]["stats"]);
    });
  }

  @override
  void dispose() {
    websocketInterface.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, left: 30.0),
                    child: customButton(
                        text: "VOLTAR",
                        onClick: () => controlling
                            ? toggleControls()
                            : Navigator.pop(context),
                        icon: Icons.arrow_back_rounded),
                  ),
                ],
              ),
              Container(
                child: Stack(
                  children: [
                    RtmpPlayer(
                      eventId: widget.event.id,
                      fullscreen: fullscreen,
                    ),
                    Positioned(
                      right: fullscreen ? null : 20,
                      bottom: fullscreen ? 60 : 20,
                      left: fullscreen ? 20 : null,
                      child: GestureDetector(
                        onTap: onToggleFullscreen,
                        child: Container(
                          height: 35,
                          width: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: grey.withOpacity(0.5),
                          ),
                          child: Icon(
                            fullscreen
                                ? Icons.fullscreen_exit
                                : Icons.fullscreen,
                            color: white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              widget.event.stats != null
                  ? Column(
                      children: [
                        SizedBox(height: 20),
                        // Text("Campeonato Nacional | Queluz, Portugal",
                        //     style: gotham_medium_small),
                        // SizedBox(height: 10),
                        // Text("Dom 22 Nov | 17:00", style: gotham_medium_small),
                        SizedBox(
                          height: 20,
                        ),
                        eventScoreboard(event: widget.event),
                        SizedBox(height: 20),
                        if (!controlling)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              customButton(
                                  text: "EDITAR",
                                  onClick: toggleControls,
                                  icon: Icons.play_arrow_rounded),
                              SizedBox(width: 10),
                              customButton(
                                  text: "GRAVAR",
                                  onClick: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => RecordScreen(
                                              eventId: widget.event.id))),
                                  icon: Icons.adjust),
                            ],
                          ),
                        SizedBox(height: 10),
                        if (controlling)
                          gameControls(
                              eventID: widget.event.id,
                              sportName: widget.event.sport.name,
                              dropdownValue1: dropdownValue1,
                              dropdownValue2: dropdownValue2,
                              onChangeDropdown1: onChangeDropdown1,
                              onChangeDropdown2: onChangeDropdown2),
                        SizedBox(height: 20),
                        stats(
                            handler: sendUpdatedStat,
                            stats: widget.event.stats,
                            controlling: controlling,
                            refresh: () => setState(() {})),
                      ],
                    )
                  : SizedBox(
                      height: 300,
                      child: Center(
                          child: Container(child: CircularProgressIndicator())),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
