import 'package:eagle_stream/models/statistics.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

bool _isNumeric(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}

Widget stats(
    {@required Statistics stats,
    @required bool controlling,
    @required Function refresh,
    @required Function handler}) {
  return Column(
    children: [
      Text("ESTATÍSTICAS", style: gotham_book_medium),
      SizedBox(
        height: 20,
      ),
      for (int i = 0; i < stats.statsNames.length; i++)
        if (!_isNumeric(stats.statsNames[i]) &&
            !(stats.statsNames[i] == "advantage"))
          Column(
            children: [
              Text(
                  '${stats.statsNames[i][0].toUpperCase()}${stats.statsNames[i].substring(1)}',
                  style: gotham_book_small),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    if (controlling)
                      Expanded(
                        child: Row(
                          children: [
                            // Expanded(child: SizedBox()),
                            GestureDetector(
                              onTap: () {
                                handler(
                                    -1, stats.statsNames[i], stats.team1Name);
                                refresh();
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  color: salmon,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Icon(
                                  Icons.remove,
                                  color: white,
                                ),
                              ),
                            ),
                            SizedBox(width: 15),

                            GestureDetector(
                              onTap: () {
                                handler(
                                    1, stats.statsNames[i], stats.team1Name);
                                refresh();
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  color: salmon,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Icon(Icons.add, color: white),
                              ),
                            ),
                            Expanded(child: SizedBox()),
                          ],
                        ),
                      ),
                    if (!controlling)
                      LinearPercentIndicator(
                        width: 100,
                        percent:
                            stats.statsTeam2[i] <= 0 || stats.statsTeam1[i] <= 0
                                ? 0
                                : stats.statsTeam1[i].toDouble() /
                                    (stats.statsTeam1[i].toDouble() +
                                        stats.statsTeam2[i].toDouble()),
                        backgroundColor: light_grey,
                        lineHeight: 8,
                        progressColor: red,
                      ),
                    Text(
                      stats.statsTeam1[i].toString() +
                          " - " +
                          stats.statsTeam2[i].toString(),
                      style: gotham_bold_medium,
                    ),
                    if (!controlling)
                      LinearPercentIndicator(
                        width: 100,
                        percent:
                            stats.statsTeam2[i] <= 0 || stats.statsTeam1[i] <= 0
                                ? 0
                                : stats.statsTeam2[i].toDouble() /
                                    (stats.statsTeam1[i].toDouble() +
                                        stats.statsTeam2[i].toDouble()),
                        backgroundColor: light_grey,
                        lineHeight: 8,
                        progressColor: orange,
                      ),
                    if (controlling)
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(child: SizedBox()),
                            GestureDetector(
                              onTap: () {
                                handler(
                                    1, stats.statsNames[i], stats.team2Name);
                                refresh();
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  color: salmon,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Icon(Icons.add, color: white),
                              ),
                            ),
                            SizedBox(width: 15),
                            GestureDetector(
                              onTap: () {
                                handler(
                                    -1, stats.statsNames[i], stats.team2Name);
                                refresh();
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  color: salmon,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Icon(Icons.remove, color: white),
                              ),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
              SizedBox(height: 30),
            ],
          )
    ],
  );
}
