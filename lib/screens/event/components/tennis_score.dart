import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget tennisScore({@required Event event}) {
  var statsNames = event.stats.statsNames;
  var stats1 = event.stats.statsTeam1;
  var stats2 = event.stats.statsTeam2;
  var advantage = statsNames.indexOf("advantage");
  var t1adv = advantage == -1 ? false : stats1[advantage] == 1;
  var t2adv = advantage == -1 ? false : stats2[advantage] == 1;
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                      color: salmon.withOpacity(t1adv ? 1 : 0),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 10),
                  Container(
                    width: 60,
                    child: Text(
                      event.team1.name,
                      style: gotham_bold_small,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Container(
                    width: 10,
                    decoration: BoxDecoration(
                      color: salmon.withOpacity(t2adv ? 1 : 0),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 10),
                  Container(
                    width: 60,
                    child: Text(
                      event.team2.name,
                      style: gotham_bold_small,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(width: 20),
          Column(
            children: [
              Text(
                "PONTOS",
                style: gotham_book_small,
              ),
              SizedBox(height: 20),
              Text(
                stats1[statsNames.indexOf("score")].toString(),
                style: gotham_bold_medium,
              ),
              SizedBox(height: 15),
              Text(
                stats2[statsNames.indexOf("score")].toString(),
                style: gotham_bold_medium,
              )
            ],
          ),
          SizedBox(width: 20),
          Column(
            children: [
              Text(
                "GAMES",
                style: gotham_book_small,
              ),
              SizedBox(height: 20),
              Text(
                stats1[statsNames.indexOf("games")].toString(),
                style: gotham_bold_medium,
              ),
              SizedBox(height: 15),
              Text(
                stats2[statsNames.indexOf("games")].toString(),
                style: gotham_bold_medium,
              )
            ],
          ),
          SizedBox(width: 20),
          if (statsNames.indexOf("0") != -1)
            Column(
              children: [
                Text(
                  "SETS",
                  style: gotham_book_small,
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    Row(
                      children: [
                        Column(
                          children: [
                            Text(
                              stats1[statsNames.indexOf("0")].toString(),
                              style: gotham_bold_medium,
                            ),
                            SizedBox(height: 20),
                            Text(
                              stats2[statsNames.indexOf("0")].toString(),
                              style: gotham_bold_medium,
                            ),
                          ],
                        ),
                      ],
                    ),
                    if (statsNames.indexOf("1") != -1)
                      Row(
                        children: [
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                stats1[statsNames.indexOf("1")].toString(),
                                style: gotham_bold_medium,
                              ),
                              SizedBox(height: 20),
                              Text(
                                stats2[statsNames.indexOf("1")].toString(),
                                style: gotham_bold_medium,
                              ),
                            ],
                          ),
                        ],
                      ),
                    if (statsNames.indexOf("2") != -1)
                      Row(
                        children: [
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                stats1[statsNames.indexOf("2")].toString(),
                                style: gotham_bold_medium,
                              ),
                              SizedBox(height: 20),
                              Text(
                                stats2[statsNames.indexOf("2")].toString(),
                                style: gotham_bold_medium,
                              ),
                            ],
                          ),
                        ],
                      ),
                  ],
                )
              ],
            )
        ],
      ),
    ],
  );
}
