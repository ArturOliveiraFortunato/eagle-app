import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/team_emblem.dart';
import 'package:flutter/material.dart';

Widget teamScore({@required Event event}) {
  int index = event.stats.statsNames.indexOf("score");
  String score1 = event.stats.statsTeam1[index].toString();
  String score2 = event.stats.statsTeam2[index].toString();
  return Row(
    mainAxisSize: MainAxisSize.max,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      TeamEmblem(teamName: event.team1.name, size: 100),
      SizedBox(width: 10),
      Column(
        children: [
          SizedBox(height: 10),
          Text(score1 + " - " + score2, style: gotham_bold_big),
        ],
      ),
      SizedBox(width: 10),
      TeamEmblem(teamName: event.team2.name, size: 100),
    ],
  );
}
