import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/screens/event/components/judo_score.dart';
import 'package:eagle_stream/screens/event/components/team_score.dart';
import 'package:eagle_stream/screens/event/components/tennis_score.dart';
import 'package:flutter/material.dart';

Widget eventScoreboard({@required Event event}) {
  if (["tennis", "padel"].contains(event.sport.name))
    return tennisScore(event: event);
  if (event.sport.name == "judo") return judoScore(event: event);
  return teamScore(event: event);
}
