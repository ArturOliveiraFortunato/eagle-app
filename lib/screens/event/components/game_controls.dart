import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

Widget gameControls(
    {@required eventID,
    @required String sportName,
    @required String dropdownValue1,
    @required String dropdownValue2,
    @required Function onChangeDropdown1,
    @required Function onChangeDropdown2}) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          customButton(
              text: "INICIAR",
              onClick: () => ServerInterface.getInstance().startEvent(eventID),
              icon: Icons.play_arrow_rounded),
          SizedBox(width: 10),
          customButton(text: "PARAR", onClick: () => {}, icon: Icons.pause),
        ],
      ),
      SizedBox(
        height: 20,
      ),
      // if (sportName == "football") // means it is not a team event
      //   Column(
      //     children: [
      //       customButton(text: "PROLONGAMENTO", onClick: () => {}),
      //       dropdown(
      //           value: "1ª PARTE",
      //           items: <String>["1ª PARTE", "2ª PARTE"],
      //           onChanged: onChangeDropdown1),
      //     ],
      //   ),
      // if (sportName == "volleyball") // means it is not a team event
      //   dropdown(
      //       value: "1º PERÍODO",
      //       items: <String>[
      //         "1º PERÍODO",
      //         "2º PERÍODO",
      //         "3º PERÍODO",
      //         "4º PERÍODO"
      //       ],
      //       onChanged: onChangeDropdown1),
      // if (sportName == "tennis")
      //   Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       dropdown(
      //           value: "1º SET",
      //           items: <String>["1º SET", "2º SET", "3º SET", "4º SET"],
      //           onChanged: onChangeDropdown1),
      //       SizedBox(width: 10),
      //       dropdown(
      //           value: "1º GAME",
      //           items: <String>["1º GAME", "2º GAME", "3º GAME"],
      //           onChanged: onChangeDropdown2),
      //     ],
      //   )
    ],
  );
}
