import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget judoScore({@required Event event}) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.access_time_sharp,
            color: white,
            size: 20,
          ),
          SizedBox(
            width: 5,
          ),
          Text("1º | 3:40", style: gotham_book_small),
        ],
      ),
      SizedBox(height: 20),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                      color: orange,
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(event.team1.name, style: gotham_bold_medium),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                      color: red,
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(event.team2.name, style: gotham_bold_medium),
                ],
              )
            ],
          ),
          SizedBox(width: 20),
          Column(
            children: [
              Text(
                "PONTOS",
                style: gotham_book_small,
              ),
              SizedBox(height: 10),
              Text(
                event.stats.statsTeam1[0].toString(),
                style: gotham_bold_medium,
              ),
              SizedBox(height: 20),
              Text(
                event.stats.statsTeam2[0].toString(),
                style: gotham_bold_medium,
              )
            ],
          )
        ],
      ),
    ],
  );
}
