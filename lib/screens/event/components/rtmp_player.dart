import 'package:flutter/material.dart';

import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

/*class RtmpPlayer extends StatelessWidget {
  String eventId;
  bool fullscreen;
  //RtmpPlayer({Key key, this.eventId, this.fullscreen}) : super(key: key);

  RtmpPlayer({Key key, String eventId, bool fullscreen}) {
    this.eventId = eventId;
    this.fullscreen = fullscreen;

    urlToStreamVideo = 'rtmp://rtmp.eagle-stream.com:1935/live/' + eventId;

    vlcPlayer = new VlcPlayer(
      aspectRatio: 9 / 16,
      url: getUrl(),
      controller: controller,
      placeholder: Center(child: CircularProgressIndicator()),
    );
  }

  bool isPlaying = true;

  String urlToStreamVideo;

  double fullHeight;
  double fullWidth;
  double portraitPlayerHeight;

  double height;
  double width;

  VlcPlayerController controller;

  static VlcPlayerController controller = new VlcPlayerController(
    // Start playing as soon as the video is loaded.
    onInit: () {
      controller.play();
    },
  );

  String getUrl() {
    return urlToStreamVideo;
  }

  void setFullscreen(bool newValue) {
    fullscreen = newValue;
  }

  VlcPlayer vlcPlayer;

  @override
  Widget build(BuildContext context) {
    fullWidth = MediaQuery.of(context).size.width;
    fullHeight = MediaQuery.of(context).size.height;
    portraitPlayerHeight = 9 * fullWidth / 16;

    width = fullscreen ? fullWidth - 2 : portraitPlayerHeight - 2;
    height = fullscreen ? fullHeight : fullWidth;

    return new SafeArea(
      child: Column(
        children: [
          RotatedBox(
            quarterTurns: fullscreen ? 0 : -1,
            child: SizedBox(
                height: height,
                width: width,
                child: RotatedBox(
                  quarterTurns: 1,
                  child: VlcPlayer(
                    controller: _videoPlayerController,
                    aspectRatio: 16 / 9,
                    placeholder: Center(child: CircularProgressIndicator()),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}*/



class RtmpPlayer extends StatefulWidget {
  String eventId;
  bool fullscreen;
  String urlToStreamVideo;

  RtmpPlayer({Key key, String eventId, bool fullscreen}) {
    this.eventId = eventId;
    this.fullscreen = fullscreen;

    this.urlToStreamVideo = 'rtmp://rtmp.eagle-stream.com:1935/live/' + eventId;

  }

  @override
  _RtmpPlayerState createState() => _RtmpPlayerState();
}

class _RtmpPlayerState extends State<RtmpPlayer> {
  VlcPlayerController _videoPlayerController;

  // =============================
  bool isPlaying = true;

  String urlToStreamVideo;

  double fullHeight;
  double fullWidth;
  double portraitPlayerHeight;

  double height;
  double width;
  // ============================

  Future<void> initializePlayer() async {}

  @override
  void initState() {
    super.initState();

    _videoPlayerController = VlcPlayerController.network(
      widget.urlToStreamVideo,
      hwAcc: HwAcc.FULL,
      autoPlay: true,
      options: VlcPlayerOptions(),
    );
  }

  @override
  void dispose() {
    _videoPlayerController.stopRendererScanning();
    _videoPlayerController.removeListener(() {});
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    fullWidth = MediaQuery.of(context).size.width;
    fullHeight = MediaQuery.of(context).size.height;
    portraitPlayerHeight = 9 * fullWidth / 16;

    width = widget.fullscreen ? fullWidth - 2 : portraitPlayerHeight - 2;
    height = widget.fullscreen ? fullHeight : fullWidth;

    return new SafeArea(
      child: Column(
        children: [
          RotatedBox(
            quarterTurns: widget.fullscreen ? 0 : -1,
            child: SizedBox(
                height: height,
                width: width,
                child: RotatedBox(
                  quarterTurns: 1,
                  child: VlcPlayer(
                    controller: _videoPlayerController,
                    aspectRatio: 16 / 9,
                    placeholder: Center(child: CircularProgressIndicator()),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
