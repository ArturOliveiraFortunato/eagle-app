import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/screens/events/components/event_row.dart';
import 'package:eagle_stream/screens/events/components/main_event_info.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/no_events.dart';
import 'package:flutter/material.dart';

class EventsScreen extends StatelessWidget {
  final List<Event> events;
  final Event mainEvent;

  EventsScreen({
    Key key,
    @required this.events,
    @required this.mainEvent,
  }) : super(key: key);

  List<Event> filterByDate(DateTime from, DateTime to) {
    if (events == null) {
      return [];
    }
    List<Event> filteredEvents = [];
    for (Event event in events) {
      if (event.date.isAfter(from) && event.date.isBefore(to)) {
        filteredEvents.add(event);
      }
    }
    return filteredEvents;
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();

    Map<String, List<Event>> filteredByDates = {
      "today": filterByDate(now.subtract(Duration(hours: now.hour)),
          now.add(Duration(days: 1, hours: -now.hour))),
      "nextDays": filterByDate(
          now.add(Duration(
            days: 1,
            hours: -now.hour,
          )),
          now.add(Duration(days: 100))),
      "yesterday": filterByDate(
          now.subtract(Duration(days: 1, hours: now.hour)),
          now.subtract(Duration(hours: now.hour))),
      "lastWeek": filterByDate(now.subtract(Duration(days: 7, hours: now.hour)),
          now.subtract(Duration(days: 1, hours: now.hour))),
    };

    return events != null
        ? SingleChildScrollView(
            child: Column(
            children: [
              mainEvent != null
                  ? mainEventInfo(mainEvent, context)
                  : Container(),
              SizedBox(height: 30),
              Text(
                "HOJE",
                style: gotham_bold_medium,
              ),
              SizedBox(height: 20),
              filteredByDates["today"].length > 0
                  ? eventRow(events: filteredByDates["today"], context: context)
                  : noEvents("NÃO EXISTEM EVENTOS NESTA DATA"),
              SizedBox(height: 30),
              Text(
                "PRÓXIMOS DIAS",
                style: gotham_bold_medium,
              ),
              SizedBox(height: 20),
              filteredByDates["nextDays"].length > 0
                  ? eventRow(
                      events: filteredByDates["nextDays"], context: context)
                  : noEvents("NÃO EXISTEM EVENTOS NESTA DATA"),
              SizedBox(height: 30),
              Text(
                "ONTEM",
                style: gotham_bold_medium,
              ),
              SizedBox(height: 20),
              filteredByDates["yesterday"].length > 0
                  ? eventRow(
                      events: filteredByDates["yesterday"], context: context)
                  : noEvents("NÃO EXISTEM EVENTOS NESTA DATA"),
              SizedBox(height: 30),
              Text(
                "NA SEMANA PASSADA",
                style: gotham_bold_medium,
              ),
              SizedBox(height: 20),
              filteredByDates["lastWeek"].length > 0
                  ? eventRow(
                      events: filteredByDates["lastWeek"], context: context)
                  : noEvents("NÃO EXISTEM EVENTOS NESTA DATA"),
              SizedBox(height: 30),
            ],
          ))
        : Center(child: Container(child: CircularProgressIndicator()));
  }
}
