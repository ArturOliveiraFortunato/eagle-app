import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/screens/event/event_screen.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/utils/consts.dart';
import 'package:eagle_stream/widgets/logo.dart';
import 'package:eagle_stream/widgets/player_name.dart';
import 'package:eagle_stream/widgets/team_emblem.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

String printHour(DateTime time) {
  String hour =
      time.hour < 10 ? "0" + time.hour.toString() : time.hour.toString();
  String minute =
      time.minute < 10 ? "0" + time.minute.toString() : time.minute.toString();
  return hour + ":" + minute;
}

Widget mainEventInfo(Event mainEvent, BuildContext context) {
  return Container(
    height: 400,
    child: Stack(
      alignment: Alignment.center,
      children: [
        mainEvent != null
            ? Container(
                height: 350,
                child: Image.network(
                  "https://eagle-stream-images.s3.eu-west-3.amazonaws.com/sports/" +
                      mainEvent.sport.name.toLowerCase() +
                      ".jpg",
                  fit: BoxFit.cover,
                ),
              )
            : CircularProgressIndicator(),
        Container(
          height: 350.0,
          decoration: BoxDecoration(
              color: Colors.white,
              gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.grey.withOpacity(0.0),
                  Colors.black,
                ],
              )),
        ),
        Positioned(
          top: 40,
          left: 30,
          child: logo(
            height: 60,
          ),
        ),
        Positioned(
          top: 140,
          child: Row(
            children: [
              if (["volleyball", "football", "basketball"]
                  .contains(mainEvent.sport.name))
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TeamEmblem(
                        teamName: mainEvent.team1.name.toLowerCase(), size: 70),
                    SizedBox(width: 70),
                    TeamEmblem(
                        teamName: mainEvent.team2.name.toUpperCase(), size: 70),
                  ],
                ),
              if (["tennis", "judo", "padel"].contains(mainEvent.sport.name))
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    playerName(name: mainEvent.team1.name, size: 100),
                    SizedBox(width: 70),
                    playerName(name: mainEvent.team2.name, size: 100),
                  ],
                ),
            ],
          ),
        ),
        Positioned(
            top: 140,
            child: Column(
              children: [
                mainEvent.live
                    ? Row(
                        children: [
                          customButton(
                            text: "LIVE",
                            onClick: null,
                            icon: Icons.fiber_manual_record,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      )
                    : Container(),
              ],
            )),
        Positioned(
          bottom: 120,
          child: Column(
            children: [
              Text(printHour(mainEvent.date), style: gotham_medium_bigger),
              SizedBox(
                height: 5,
              ),
              Text("${mainEvent.date.day} ${months[mainEvent.date.month]}",
                  style: gotham_medium_big),
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          child: customButton(
            onClick: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => EventScreen(event: mainEvent)),
            ),
            icon: Icons.play_arrow_rounded,
            text: "ASSISTIR AGORA",
          ),
        ),
      ],
    ),
  );
}
