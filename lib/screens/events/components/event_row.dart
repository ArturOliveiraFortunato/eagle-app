 import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/widgets/event_option.dart';
import 'package:flutter/material.dart';

Widget eventRow(
    {@required List<Event> events, @required BuildContext context}) {
  return Column(
    children: [
      Container(
        height: 150,
        child: Stack(
          children: [
            ListView(
              scrollDirection: Axis.horizontal,
              children: [
                for (int i = 0; i < events.length; i++)
                  Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: eventOption(
                        event: events[i], big: false, context: context),
                  ),
              ],
            ),
            Positioned(
              right: 0,
              child: Container(
                height: 150.0,
                width: 50,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: FractionalOffset.center,
                  end: FractionalOffset.centerRight,
                  colors: [
                    Colors.black.withOpacity(0.0),
                    Colors.black,
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
