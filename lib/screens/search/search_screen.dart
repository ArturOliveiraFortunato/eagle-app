import 'package:eagle_stream/screens/search/components/sports_selector.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/event_option.dart';
import 'package:eagle_stream/widgets/no_events.dart';
import 'package:flutter/material.dart';
import 'package:eagle_stream/models/event.dart';

const Map<String, IconData> iconList = {
  "volleyball": Icons.sports_volleyball_outlined,
  "basketball": Icons.sports_basketball_outlined,
  "tennis": Icons.sports_tennis,
  "padel": Icons.sports_tennis, //FIXME change icon
  "football": Icons.sports_soccer_outlined,
  //"judo": Icons.sports_kabaddi,
};

class Search extends StatefulWidget {
  /// map {sport}->{location->[event]}
  final List<Event> events;

  Search({
    Key key,
    @required this.events,
  }) : super(key: key);

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  String selectedSport = "volleyball";
  //String selectedZone;
  bool choosingZone = false;
  List<Event> selectedEvents = [];

  @override
  void initState() {
    super.initState();
    filterEvents();
    //selectedZone = widget.events[selectedSport].keys.toList()[0];
  }

  void onSelectSport(String sport) {
    setState(() {
      selectedSport = sport;
    });
    filterEvents();
  }

  // void onSelectZone(String zone) {
  //   setState(() {
  //     selectedZone = zone;
  //   });
  // }

  void toggleChoosingZone() {
    setState(() {
      choosingZone = !choosingZone;
    });
  }

  // /// returns a list with all possible zones e.g [Lisboa, Porto, Coimbra]
  // List<String> filterZones() {
  //   List<String> zones = List<String>();
  //   for (Map m in widget.events.values) {
  //     for (String zone in m.keys) {
  //       if (!zones.contains(zone)) {
  //         zones.add(zone);
  //       }
  //     }
  //   }
  //   return zones;
  // }

  void filterEvents({String team}) {
    List<Event> filteredEvents = [];
    for (Event event in widget.events) {
      if (event.sport.name == selectedSport) if (team != null) {
        if (event.team1.name == team || event.team2.name == team)
          filteredEvents.add(event);
      } else {
        filteredEvents.add(event);
      }
    }
    if (mounted) {
      setState(() {
        selectedEvents = filteredEvents;
      });
    } else {
      selectedEvents = filteredEvents;
    }
  }

  // Map<String, int> buildZonesEventCount() {
  //   Map<String, int> zonesEventCount = Map<String, int>();
  //   var events = widget.events[selectedSport];
  //   for (int i = 0; i < events.length; i++) {
  //     zonesEventCount[events.keys.toList()[i]] =
  //         events.values.toList()[i].length;
  //   }
  //   print(zonesEventCount);
  //   return zonesEventCount;
  // }

  // //TODO listview instead of for
  // Widget showOptions() {
  //   if (widget.events[selectedSport] == []) {
  //     return Text("Não há jogos de $selectedSport disponíveis");
  //   } else {
  //     return Column(children: [
  //       for (int i = 0; i < widget.events[selectedSport].length; i++)
  //         Text(widget.events[selectedSport].keys.toList()[i])
  //     ]);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    print(selectedEvents.length);
    return widget.events != null
        ? SafeArea(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  "DO QUE ESTÁS À PROCURA?",
                  style: gotham_book_medium,
                ),
                SizedBox(height: 10),
                Divider(
                  height: 3,
                  thickness: 2,
                  color: light_grey,
                  indent: 40,
                  endIndent: 40,
                ),
                SizedBox(height: 20),
                Text(selectedSport.toUpperCase(), style: gotham_book_medium),
                SizedBox(height: 10),
                if (selectedEvents.length != 0)
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          for (int i = 0; i < selectedEvents.length; i++)
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: eventOption(
                                  event: selectedEvents[i],
                                  big: true,
                                  context: context),
                            )
                        ],
                      ),
                    ),
                  ),
                if (selectedEvents.length == 0)
                  Expanded(
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        noEvents("NÃO EXISTEM JOGOS DISPONÍVEIS"),
                      ],
                    ),
                  ),
                SizedBox(height: 10),
                SportsSelector(
                    onSelectSport: onSelectSport,
                    iconList: iconList,
                    selectedSport: selectedSport),
                SizedBox(height: 10),
              ],
            ),
          )
        : Center(child: Container(child: CircularProgressIndicator()));
  }
}
