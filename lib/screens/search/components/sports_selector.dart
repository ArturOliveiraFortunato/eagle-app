import 'package:eagle_stream/style/custom_colors.dart';
import 'package:flutter/material.dart';

class SportsSelector extends StatelessWidget {
  final Function onSelectSport;
  final Map<String, IconData> iconList;
  final String selectedSport;

  SportsSelector({
    Key key,
    @required this.onSelectSport,
    @required this.iconList,
    @required this.selectedSport,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
      child: Container(
        decoration: BoxDecoration(
          color: dark,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            for (int i = 0; i < iconList.length; i++)
              IconButton(
                onPressed: () => onSelectSport(iconList.keys.toList()[i]),
                icon: Icon(
                  iconList.values.toList()[i],
                  size: 30,
                  color: selectedSport == iconList.keys.toList()[i]
                      ? salmon
                      : light_grey,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
