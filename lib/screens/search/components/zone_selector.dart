import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

class ZonesSelector extends StatelessWidget {
  final Function onSelect;
  final Function onClick;
  final List<String> zones;
  final String selectedZone;
  final bool choosingZone;
  final Map<String, int> zonesEventCount;

  ZonesSelector({
    Key key,
    @required this.onSelect,
    @required this.onClick,
    @required this.zones,
    @required this.selectedZone,
    @required this.choosingZone,
    @required this.zonesEventCount,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30),
      child: Container(
        decoration: BoxDecoration(
          color: dark,
          borderRadius: BorderRadius.circular(10),
        ),
        constraints: BoxConstraints(
          maxHeight: 400,
          minHeight: 30,
        ),
        child: choosingZone
            ? SingleChildScrollView(
                child: Column(
                  children: [
                    for (int i = 0; i < zones.length; i++)
                      GestureDetector(
                        onTap: () {
                          onSelect(zones[i]);
                          onClick();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                zones[i] +
                                    " (" +
                                    zonesEventCount[zones[i]].toString() +
                                    ")",
                                style: gotham_medium_big.copyWith(
                                    color: zones[i] == selectedZone
                                        ? salmon
                                        : light_grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              )
            : GestureDetector(
                onTap: () {
                  onClick();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        selectedZone +
                            " (" +
                            zonesEventCount[selectedZone].toString() +
                            ")",
                        style: gotham_medium_big.copyWith(color: salmon),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
