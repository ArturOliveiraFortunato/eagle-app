import 'package:eagle_stream/models/user.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:eagle_stream/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';

class EditProfileScreen extends StatefulWidget {
  final User user;
  final Function onRefreshProfile;

  EditProfileScreen({
    Key key,
    @required this.user,
    @required this.onRefreshProfile,
  }) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  String newUsername;
  String newEmail;
  String newPassword = "";
  String oldPassword = "";

  bool matchingPassword = true;

  ServerInterface serverInterface = ServerInterface.getInstance();

  void onSaveChanges() {
    serverInterface
        .changeProfile(
            newUsername: newUsername,
            newEmail: newEmail,
            newPassword: newPassword,
            oldPassword: oldPassword)
        .then((value) {
      widget.onRefreshProfile();
    });
  }

  @override
  void initState() {
    super.initState();
    newUsername = widget.user.name;
    newEmail = widget.user.email;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 80),
                SizedBox(height: 30),
                Text("NOME", style: gotham_book_medium),
                SizedBox(height: 10),
                customTextFiled(
                    hint: "NOME",
                    text: newUsername,
                    hideText: false,
                    onChanged: (String value) {
                      setState(() {
                        newUsername = value;
                      });
                    }),
                // SizedBox(height: 20),
                // Text("EMAIL", style: gotham_book_medium),
                // SizedBox(height: 10),
                // customTextFiled(
                //     hint: "EMAIL",
                //     text: newEmail,
                //     hideText: false,
                //     onChanged: (String value) {
                //       setState(() {
                //         newEmail = value;
                //       });
                //     }),
                // SizedBox(height: 20),
                // Text("ANTIGA PALAVRA-CHAVE", style: gotham_book_medium),
                // SizedBox(height: 10),
                // customTextFiled(
                //     hint: "PALAVRA-CHAVE",
                //     text: oldPassword,
                //     hideText: true,
                //     onChanged: (String value) {
                //       setState(() {
                //         oldPassword = value;
                //       });
                //     }),
                // SizedBox(height: 20),
                // Text("NOVA PALAVRA-CHAVE", style: gotham_book_medium),
                // SizedBox(height: 10),
                // customTextFiled(
                //     hint: "PALAVRA-CHAVE",
                //     text: newPassword,
                //     hideText: true,
                //     onChanged: (String value) {
                //       setState(() {
                //         newPassword = value;
                //       });
                //     }),
                // SizedBox(height: 20),
                // if (!matchingPassword)
                //   Text(
                //     "PALAVRAS-PASS TÊM DE SER IGUAIS",
                //     style: gotham_medium_small.copyWith(color: salmon),
                //   ),
                // Text("CONFIRMAR PALAVRA-CHAVE", style: gotham_book_medium),
                // SizedBox(height: 10),
                // customTextFiled(
                //     hint: "PALAVRA-CHAVE",
                //     text: "",
                //     hideText: true,
                //     onChanged: (String value) {
                //       setState(() {
                //         matchingPassword = newPassword == value;
                //       });
                //     }),
                SizedBox(height: 30),
                Align(
                  alignment: Alignment.center,
                  child: customButton(
                    icon: Icons.done,
                    text: "GUARDAR ALTERAÇÕES",
                    onClick: () {
                      onSaveChanges();
                      Navigator.pop(context);
                    },
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
