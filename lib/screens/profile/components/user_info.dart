import 'package:eagle_stream/models/user.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:flutter/material.dart';

Widget userInfo({@required User user}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    //info
    children: [
      Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(user.name, style: gotham_bold_medium),
              SizedBox(height: 10),
              Row(
                children: [
                  Icon(Icons.mail, color: white, size: 15),
                  SizedBox(width: 5),
                  Text(user.email, style: gotham_book_small),
                ],
              ),
            ],
          ),
        ],
      )
    ],
  );
}
