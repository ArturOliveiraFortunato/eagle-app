import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

Widget profileHeader({@required Function onEdit, @required onMore}) {
  return Padding(
    padding: const EdgeInsets.all(12.0),
    child: Padding(
      padding: const EdgeInsets.only(right: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        // header
        children: [
          customButton(icon: Icons.edit, text: "EDITAR", onClick: onEdit),
        ],
      ),
    ),
  );
}
