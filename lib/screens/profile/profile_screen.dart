import 'package:eagle_stream/models/event.dart';
import 'package:eagle_stream/models/user.dart';
import 'package:eagle_stream/screens/events/components/event_row.dart';
import 'package:eagle_stream/screens/profile/components/profile_header.dart';
import 'package:eagle_stream/screens/profile/edit_profile_screen.dart';
import 'package:eagle_stream/services/serverInterface.dart';
import 'package:eagle_stream/style/custom_colors.dart';
import 'package:eagle_stream/style/typography.dart';
import 'package:eagle_stream/widgets/custom_button.dart';
import 'package:flutter/material.dart';

import 'components/user_info.dart';

class Profile extends StatefulWidget {
  final User user;
  final Function onLogout;
  final Function onRefreshProfile;

  Profile({
    Key key,
    @required this.user,
    @required this.onLogout,
    @required this.onRefreshProfile,
  }) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool editing = false;
  List<Event> recordedEvents;

  final ServerInterface serverInterface = ServerInterface.getInstance();

  void showEditScreen() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditProfileScreen(
                  user: widget.user,
                  onRefreshProfile: widget.onRefreshProfile,
                )));
  }

  @override
  void initState() {
    super.initState();
    serverInterface.getMyEvents().then((fetchedEvents) => {
          setState(() {
            recordedEvents = fetchedEvents;
          })
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: widget.user != null
          ? SingleChildScrollView(
              child: Column(
                children: [
                  profileHeader(onEdit: showEditScreen, onMore: () {}),
                  SizedBox(height: 20),
                  userInfo(user: widget.user),
                  SizedBox(height: 40),
                  Text("JOGOS GRAVADOS", style: gotham_bold_medium),
                  SizedBox(height: 20),
                  (recordedEvents == null || recordedEvents.isEmpty)
                      ? Padding(
                          padding: const EdgeInsets.only(top: 40, bottom: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  height: 60,
                                  child: Image.asset(
                                      'assets/images/no-games.png')),
                              SizedBox(width: 10),
                              Text("AINDA NÃO GRAVASTE JOGOS",
                                  style:
                                      gotham_bold_small.copyWith(color: grey)),
                            ],
                          ),
                        )
                      : eventRow(events: recordedEvents, context: context),
                  SizedBox(height: 20),
                  // Text("JOGOS ASSISTIDOS", style: gotham_bold_medium),
                  // SizedBox(height: 20),
                  // (widget.watchedEvents == null || widget.watchedEvents.isEmpty)
                  //     ? Padding(
                  //         padding: const EdgeInsets.only(top: 40, bottom: 40),
                  //         child: Row(
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           children: [
                  //             Container(
                  //                 height: 60,
                  //                 child: Image.asset(
                  //                     'assets/images/no-games.png')),
                  //             SizedBox(width: 10),
                  //             Text("AINDA NÃO VISTE JOGOS",
                  //                 style:
                  //                     gotham_bold_small.copyWith(color: grey)),
                  //           ],
                  //         ),
                  //       )
                  //     : eventRow(
                  //         events: widget.watchedEvents, context: context),
                  // SizedBox(height: 30),
                  customButton(
                      text: 'LOGOUT',
                      icon: Icons.logout,
                      onClick: () {
                        widget.onLogout();
                      }),
                  SizedBox(height: 20),
                ],
              ),
            )
          : Center(child: Container(child: CircularProgressIndicator())),
    );
  }
}
