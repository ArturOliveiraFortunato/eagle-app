import 'package:flutter/material.dart';

class ShowSnackBarAnimation {
  static void showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, String message,
      bool error, int duration) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: error ? Colors.red : Colors.green,
      content: Text(message),
      duration: Duration(seconds: duration),
    ));
  }
}
